<?php

/**
 * @file
 * CleverReach module route files.
 */

const CLEVERREACH_NAME = 'CleverReach®';
const CLEVERREACH_DESCRIPTION = 'Configure settings for CleverReach® integration.';
const CLEVERREACH_CONFIG_WEIGHT = -15;
const CLEVERREACH_CONFIG_POSITION = 'right';
const CLEVERREACH_ROLE = 'administer cleverreach';
const CLEVERREACH_BASE_FORM_ID = 'clever_reach_execute';

/**
 * Loads all routes need for cleverreach module.
 *
 * @return array
 *   List of routes.
 */
function clever_reach_load_routes() {
  return array(
    /* Process routes. */
    'cleverreach/process' => array(
      'page callback' => 'clever_reach_start_process',
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.process.inc',
      'name' => 'cleverreach.process',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/callback' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachCallbackController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.callback',
    ),
    'admin/config/cleverreach' => array(
      'title' => CLEVERREACH_NAME,
      'description' => CLEVERREACH_DESCRIPTION,
      'position' => CLEVERREACH_CONFIG_POSITION,
      'weight' => CLEVERREACH_CONFIG_WEIGHT,
      'page callback' => 'system_admin_menu_block_page',
      'file' => 'controllers/cleverreach.base.inc',
      'access arguments' => array(CLEVERREACH_ROLE),
      'name' => 'cleverreach.menu.item',
    ),
    'admin/config/cleverreach/state' => array(
      'title' => CLEVERREACH_NAME,
      'description' => CLEVERREACH_DESCRIPTION,
      'position' => CLEVERREACH_CONFIG_POSITION,
      'weight' => CLEVERREACH_CONFIG_WEIGHT,
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachResolveStateController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.state',
    ),
    'admin/config/cleverreach/welcome' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachWelcomeController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.welcome',
      'type' => MENU_LOCAL_ACTION,
    ),
    'admin/config/cleverreach/dashboard' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachDashboardController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.dashboard',
      'type' => MENU_LOCAL_ACTION,
    ),
    'admin/config/cleverreach/initialsync' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachInitialSyncController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.initialsync',
      'type' => MENU_LOCAL_ACTION,
    ),
    'admin/config/cleverreach/tokenexpired' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachTokenExpiredController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.tokenexpired',
      'type' => MENU_LOCAL_ACTION,
    ),
    'admin/config/cleverreach/config' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        CLEVERREACH_BASE_FORM_ID,
        'CleverReachInitialSyncConfigController',
      ),
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.base.inc',
      'name' => 'cleverreach.initialsync_config',
      'type' => MENU_LOCAL_ACTION,
    ),
    /* AJAX routes. */
    'cleverreach/auth/checkstatus' => array(
      'page callback' => 'clever_reach_check_auth_status',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.ajax.inc',
      'name' => 'cleverreach.auth.check.status',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/configuration' => array(
      'page callback' => 'clever_reach_configuration',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.config.inc',
      'name' => 'cleverreach.configuration',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/import/checkstatus' => array(
      'page callback' => 'clever_reach_check_import_status',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.ajax.inc',
      'name' => 'cleverreach.import.check.status',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/wakeup' => array(
      'page callback' => 'clever_reach_wakeup',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.ajax.inc',
      'name' => 'cleverreach.wakeup',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/sync/firstemail' => array(
      'page callback' => 'clever_reach_build_first_email',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.ajax.inc',
      'name' => 'cleverreach.build.first.email',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/sync/retry' => array(
      'page callback' => 'clever_reach_sync_retry',
      'access arguments' => array(CLEVERREACH_ROLE),
      'file' => 'controllers/cleverreach.ajax.inc',
      'name' => 'cleverreach.retry.sync',
      'type' => MENU_CALLBACK,
    ),
    /* Search routes. */
    'cleverreach/search' => array(
      'page callback' => 'clever_reach_article_search',
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.article.search.inc',
      'name' => 'cleverreach.search',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/v1/items' => array(
      'page callback' => 'clever_reach_get_searchable_items_response',
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.searchable.items.inc',
      'name' => 'cleverreach.searchable.items',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/v1/%/attributes' => array(
      'page callback' => 'clever_reach_get_searchable_item_schema_response',
      'page arguments' => array(2),
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.searchable.item.schema.inc',
      'name' => 'cleverreach.searchable.item.schema',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/v1/%/search' => array(
      'page callback' => 'clever_reach_get_search_result_response',
      'page arguments' => array(2),
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.search.items.inc',
      'name' => 'cleverreach.search.items',
      'type' => MENU_CALLBACK,
    ),
    'cleverreach/v1/%/%' => array(
      'page callback' => 'clever_reach_get_search_result_response',
      'page arguments' => array(2, 3),
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.search.items.inc',
      'name' => 'cleverreach.search.id',
      'type' => MENU_CALLBACK,
    ),
    /* Event hook routes. */
    'cleverreach/hookhandler' => array(
      'page callback' => 'cleverreach_handler',
      'access callback' => TRUE,
      'file' => 'controllers/cleverreach.event.handler.inc',
      'name' => 'cleverreach.eventhandler',
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Gets URL path for provided route name.
 *
 * @param string $name
 *   Route name.
 *
 * @return string|null
 *   URL path.
 */
function clever_reach_get_route_by_name($name) {
  foreach (clever_reach_load_routes() as $path => $route) {
    if ($route['name'] === "cleverreach.$name") {
      return $path;
    }
  }

  return NULL;
}
