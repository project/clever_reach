<?php

/**
 * @file
 * Base handler of view classes and files.
 */

/**
 * Executes get or post method in view class.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 *   Array of template vars.
 *
 * @throws Exception
 */
function clever_reach_execute(array $form, array &$form_state) {
  if (!isset($form_state['build_info']['args'][0])) {
    throw new Exception('CleverReach controller class not found.');
  }

  $class = $form_state['build_info']['args'][0];
  /** @var CleverReachBaseController $instance */
  $instance = new $class();

  if ($instance->isPost()) {
    return $instance->post($form_state);
  }

  return $instance->get($form_state);
}

/**
 * Resolver Controller. Resolves current state of user synchronization.
 */
class CleverReachBaseController {

  /**
   * Executes GET request method.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    throw new Exception('Method not allowed.');
  }

  /**
   * Executes POST request method.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @throws Exception
   */
  public function post(array &$form) {
    throw new Exception('Method not allowed.');
  }

  /**
   * Checks if current method is POST.
   *
   * @return bool
   *   If method is post returns true, otherwise false.
   */
  public function isPost() {
    return $_SERVER['REQUEST_METHOD'] === 'POST';
  }

}
