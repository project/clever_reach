<?php

/**
 * @file
 * Article search endpoint.
 */

/**
 * Allows search of articles in CleverReach system.
 */
function clever_reach_article_search() {
  $result = array();
  $params = drupal_get_query_parameters($_POST);

  if (isset($params['get'])) {
    switch ($params['get']) {
      case 'filter':
        $result = clever_reach_get_filters();
        break;

      case 'search':
        $result = clever_reach_get_search_result($params);
        break;
    }
  }

  drupal_json_output($result);
}

/**
 * Searches for articles with specific search term in title and/or id.
 *
 * @param array $params
 *   Search parameters, currently supported: entity_id, title and language.
 *
 * @return array
 *   List of search results. Example:
 *   array(
 *      'title' => "Some title",
 *      'description' => "Node 1",
 *      'image' => "http://www.example.com/node/image.jpg",
 *      'content' => <p>Node 1</p>,
 *      'url' => "http://www.example.com/node/1",
 *   )
 */
function clever_reach_get_search_result(array $params) {
  module_load_include('inc', 'clever_reach', 'includes/cleverreach.article.search');

  $result = array(
    'settings' => array(
      'type' => 'content',
      'link_editable' => TRUE,
      'link_text_editable' => TRUE,
      'image_size_editable' => TRUE,
    ),
    'items' => array(),
  );

  $filter = array();
  $language = NULL;
  if (isset($params['entity_id']) && $value = $params['entity_id']) {
    $filter[] = array('field' => 'entity_id', 'value' => $value);
  }

  if (isset($params['title']) && $value = $params['title']) {
    $filter[] = array(
      'field' => 'title',
      'value' => $value,
      'condition' => 'CONTAINS',
    );
  }

  if (isset($params['language']) && $value = $params['language']) {
    $filter[] = array('field' => 'language', 'value' => $value);
    $language = $value === 'und' ? NULL : $value;
  }

  if (empty($filter)) {
    return $result;
  }

  $articleSearch = new CleverReachArticleSearch();
  foreach ($articleSearch->get($filter) as $article) {
    $url = $articleSearch->getUrlByArticle($article, $language);
    $content = $articleSearch->getContentByArticle($article, $language);

    $result['items'][] = array(
      'title' => $article->title,
      'description' => strip_tags($content),
      'image' => '',
      'content' => '<!--#html #-->' . $content . '<!--#/html#-->',
      'url' => $url,
    );
  }

  return $result;
}

/**
 * Create filters available.
 *
 * Supported filtering by entity_id, title and language.
 *
 * @return array
 *   Array of available filter in article search.
 */
function clever_reach_get_filters() {
  $result = array(
    'entity_id' => array(
      'name' => t('Article ID'),
      'description' => '',
      'required' => FALSE,
      'query_key' => 'entity_id',
      'type' => 'input',
    ),
    'title' => array(
      'name' => t('Article Title'),
      'description' => '',
      'required' => FALSE,
      'query_key' => 'title',
      'type' => 'input',
    ),
  );

  // If site is multilingual, allow filtering by language.
  if (drupal_multilingual()) {
    $result['language'] = array(
      'name' => t('Language'),
      'description' => t('Please select language.'),
      'required' => FALSE,
      'query_key' => 'language',
      'type' => 'dropdown',
    );

    $result['language']['values'][] = array(
      'text' => t('Language neutral'),
      'value' => 'und',
    );

    foreach (language_list() as $language) {
      $result['language']['values'][] = array(
        'text' => $language->name,
        'value' => $language->language,
      );
    }
  }

  return array_values($result);
}
