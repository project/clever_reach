<?php

/**
 * @file
 * V1 Searchable item schema endpoint.
 */

use CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\EnumSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\Enum;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SearchableItemSchema;

/**
 * Gets list of supported searchable item fields from Drupal system.
 *
 * @param string $contentType
 *   Content type code.
 *
 * @throws Exception
 *   When content type is not found.
 */
function clever_reach_get_searchable_item_schema_response($contentType) {
  try {
    $result = clever_reach_get_schema_by_type($contentType)->toArray();
  }
  catch (Exception $e) {
    $result = array('status' => 'error', 'message' => $e->getMessage());
  }

  drupal_json_output($result);
}

/**
 * Gets schema for provided content type.
 *
 * @param string $contentType
 *   Content type code.
 *
 * @return CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SearchableItemSchema
 *   Object containing schema objects.
 *
 * @throws Exception
 *   When content type is not found.
 */
function clever_reach_get_schema_by_type($contentType) {
  if (!array_key_exists($contentType, node_type_get_types())) {
    throw new Exception("Content type \"$contentType\" not found.");
  }

  if (!class_exists('CleverReachArticleSearchField')) {
    module_load_include(
        'inc',
        'clever_reach',
        'includes/cleverreach.article.search.field'
    );
  }

  $schema = clever_reach_get_static_schema();
  $params = array('entity_type' => 'node', 'bundle' => $contentType);

  // Function field_read_fields in drupal 7.1 doesn't support filtering
  // by content type and bundle, so both field and instances must be loaded.
  $fields = field_read_fields();
  $instances = array_column(
        field_read_instances($params),
        NULL,
        'field_name'
    );

  foreach ($fields as $fieldName => $field) {
    if (!array_key_exists($fieldName, $instances)) {
      continue;
    }

    if (!$fieldType = CleverReachArticleSearchField::getField(
        $field,
        $instances[$fieldName]
    )) {
      continue;
    }

    $schema[] = $fieldType->getSchemaField();
  }

  return new SearchableItemSchema($contentType, $schema);
}

/**
 * Gets static schema.
 *
 * @see https://www.drupal.org/node/49768
 *
 * @return CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttribute[]
 *   List of static schema attributes.
 */
function clever_reach_get_static_schema() {
  $languages = array(new Enum(t('Language neutral'), 'und'));

  foreach (language_list() as $language) {
    $languages[] = new Enum(
        $language->language,
        $language->name
    );
  }

  return array(
    new SimpleSchemaAttribute(
            'url',
            t('Url'),
            FALSE,
            array(),
            SchemaAttributeTypes::URL
    ),
    new SimpleSchemaAttribute(
            'author',
            t('Author'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            SchemaAttributeTypes::AUTHOR
    ),
    new SimpleSchemaAttribute(
            'mainImage',
            t('Main Image'),
            FALSE,
            array(),
            SchemaAttributeTypes::IMAGE
    ),
    new SimpleSchemaAttribute(
            'articleHtml',
            t('Article HTML'),
            FALSE,
            array(),
            SchemaAttributeTypes::HTML
    ),
    new SimpleSchemaAttribute(
            'title',
            t('Title'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
              Conditions::CONTAINS,
            ),
            SchemaAttributeTypes::TEXT
    ),
    new SimpleSchemaAttribute(
            'date',
            t('Date'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
              Conditions::GREATER_EQUAL,
              Conditions::GREATER_THAN,
              Conditions::LESS_EQUAL,
              Conditions::LESS_THAN,
            ),
            SchemaAttributeTypes::DATE
    ),
    new EnumSchemaAttribute(
            'status',
            t('Published'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            array(
              new Enum(TRUE, NODE_PUBLISHED),
              new Enum(FALSE, NODE_NOT_PUBLISHED),
            )
    ),
    new SimpleSchemaAttribute(
            'changed',
            t('Changed'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
              Conditions::GREATER_EQUAL,
              Conditions::GREATER_THAN,
              Conditions::LESS_EQUAL,
              Conditions::LESS_THAN,
            ),
            SchemaAttributeTypes::DATE
    ),
    new EnumSchemaAttribute(
            'comment',
            t('Comment'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            array(
              new Enum(t('Hidden'), COMMENT_NODE_HIDDEN),
              new Enum(t('Closed'), COMMENT_NODE_CLOSED),
              new Enum(t('Opened'), COMMENT_NODE_OPEN),
            )
    ),
    new EnumSchemaAttribute(
            'promote',
            t('Promote'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            array(
              new Enum(TRUE, NODE_PROMOTED),
              new Enum(FALSE, NODE_NOT_PROMOTED),
            )
    ),
    new EnumSchemaAttribute(
            'sticky',
            t('Sticky'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            array(
              new Enum(TRUE, NODE_STICKY),
              new Enum(FALSE, NODE_NOT_STICKY),
            )
    ),
    new SimpleSchemaAttribute(
            'log',
            t('Revision log message'),
            FALSE,
            array(),
            SchemaAttributeTypes::TEXT
    ),
    new SimpleSchemaAttribute(
            'revision_timestamp',
            t('Revision Timestamp'),
            FALSE,
            array(),
            SchemaAttributeTypes::DATE
    ),
    new EnumSchemaAttribute(
            'language',
            t('Language'),
            TRUE,
            array(
              Conditions::EQUALS,
              Conditions::NOT_EQUAL,
            ),
            $languages
    ),
  );
}
