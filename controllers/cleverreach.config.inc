<?php

/**
 * @file
 * Configuration endpoint.
 */

use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage;
use CleverReach\Infrastructure\ServiceRegister;

/**
 * Configuration endpoint. Return JSON encoded string.
 *
 * Route cleverreach/wakeup.
 *
 * @throws \Exception
 */
function clever_reach_configuration() {
  /** @var \CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage $queue */
  $queue = ServiceRegister::getService(TaskQueueStorage::CLASS_NAME);
  /** @var CleverReachConfigService $config */
  $config = ServiceRegister::getService(Configuration::CLASS_NAME);

  if (in_array($_SERVER['REQUEST_METHOD'], array('POST', 'PUT'), TRUE)) {
    clever_reach_update_config($config);
  }

  $items = array();
  /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $item */
  foreach ($queue->findAll() as $item) {
    $items[] = array(
      'type' => $item->getTaskType(),
      'status' => $item->getStatus(),
      'startedAt' => date('c', $item->getStartTimestamp()),
      'progress' => $item->getProgressFormatted(),
      'retries' => $item->getRetries(),
      'failure' => $item->getFailureDescription(),
    );
  }

  $config = array(
    'integrationId' => $config->getIntegrationId(),
    'integrationName' => $config->getIntegrationName(),
    'minLogLevel' => $config->getMinLogLevel(),
    'isProductSearchEnabled' => $config->isProductSearchEnabled(),
    'productSearchParameters' => $config->getProductSearchParameters(),
    'recipientsSynchronizationBatchSize' => $config->getRecipientsSynchronizationBatchSize(),
    'isDefaultLoggerEnabled' => $config->isDefaultLoggerEnabled(),
    'maxStartedTasksLimit' => $config->getMaxStartedTasksLimit(),
    'maxTaskExecutionRetries' => $config->getMaxTaskExecutionRetries(),
    'maxTaskInactivityPeriod' => $config->getMaxTaskInactivityPeriod(),
    'taskRunnerMaxAliveTime' => $config->getTaskRunnerMaxAliveTime(),
    'taskRunnerStatus' => $config->getTaskRunnerStatus(),
    'taskRunnerWakeupDelay' => $config->getTaskRunnerWakeupDelay(),
    'defaultRecipientStatus' => $config->getDefaultRecipientStatus(),
    'configuredInitialSync' => $config->isConfiguredInitialSync(),
    'queueName' => $config->getQueueName(),
    'drupalVersion' => VERSION,
    'currentQueue' => $items,
    'eventHandlerUrl' => $config->getCrEventHandlerURL(),
  );

  drupal_json_output(
        array(
          'status' => 'success',
          'config' => $config,
        )
    );
}

/**
 * Updates configuration from POST request.
 *
 * @param CleverReachConfigService $service
 *   Configuration service object.
 *
 * @throws \Exception
 */
function clever_reach_update_config(CleverReachConfigService $service) {
  $data = file_get_contents('php://input');
  $payload = drupal_json_decode($data);

  if (array_key_exists('minLogLevel', $payload)) {
    $service->saveMinLogLevel($payload['minLogLevel']);
  }
  if (array_key_exists('defaultLoggerStatus', $payload)) {
    $service->setDefaultLoggerEnabled($payload['defaultLoggerStatus']);
  }
  if (array_key_exists('maxStartedTasksLimit', $payload)) {
    $service->setMaxStartedTaskLimit($payload['maxStartedTasksLimit']);
  }
  if (array_key_exists('taskRunnerWakeUpDelay', $payload)) {
    $service->setTaskRunnerWakeUpDelay($payload['taskRunnerWakeUpDelay']);
  }
  if (array_key_exists('taskRunnerMaxAliveTime', $payload)) {
    $service->setTaskRunnerMaxAliveTime($payload['taskRunnerMaxAliveTime']);
  }
  if (array_key_exists('maxTaskExecutionRetries', $payload)) {
    $service->setMaxTaskExecutionRetries($payload['maxTaskExecutionRetries']);
  }
  if (array_key_exists('maxTaskInactivityPeriod', $payload)) {
    $service->setMaxTaskInactivityPeriod($payload['maxTaskInactivityPeriod']);
  }
  if (array_key_exists('defaultRecipientStatus', $payload)) {
    $service->setDefaultRecipientStatus($payload['defaultRecipientStatus']);
  }
  if (array_key_exists('configuredInitialSync', $payload)) {
    $service->setConfiguredInitialSync($payload['configuredInitialSync']);
  }
}
