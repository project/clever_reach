<?php

/**
 * @file
 * V1 Search item controller.
 */

use CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SearchResultItem;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\TextAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\DateAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\EnumAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\HtmlAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\ImageAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\AuthorAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\UrlAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SearchResult;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Validator;
use CleverReach\BusinessLogic\Utility\ArticleSearch\FilterParser;

/**
 * Gets list of search results performed by CleverReach system.
 *
 * @param string $contentType
 *   Content type code.
 * @param string $id
 *   Optional parameter for search by content ID.
 *
 * @throws Exception
 *   When filter string is not provided.
 */
function clever_reach_get_search_result_response($contentType, $id = NULL) {
  $params = drupal_get_query_parameters();

  try {
    if ($id === NULL && !isset($params['filter'])) {
      throw new Exception('Filter is not provided.');
    }

    module_load_include(
        'inc',
        'clever_reach',
        'includes/cleverreach.article.search'
    );
    module_load_include(
        'inc',
        'clever_reach',
        'controllers/cleverreach.searchable.item.schema'
    );
    module_load_include(
        'inc',
        'clever_reach',
        'includes/cleverreach.article.search.field'
    );

    if ($id === NULL) {
      $articleFilter = clever_reach_get_article_filter(
        $contentType,
        $params['filter']
      );
    }
    else {
      $articleFilter = array(
        array(
          'field' => 'nid',
          'value' => $id,
          'condition' => '=',
        ),
        array(
          'field' => 'type',
          'value' => $contentType,
          'condition' => '=',
        ),
      );
    }

    $result = clever_reach_get_search_result(
        $contentType,
        $articleFilter
    )->toArray();
  }
  catch (Exception $e) {
    $result = array('status' => 'error', 'message' => $e->getMessage());
  }

  drupal_json_output($result);
}

/**
 * Converts provided timestamp to DateTime object.
 *
 * @param int $timestamp
 *   Unix timestamp format.
 *
 * @return \DateTime
 *   Converted object from timestamp.
 */
function clever_reach_timestamp_to_date($timestamp) {
  $dateTime = new DateTime();
  return $dateTime->setTimestamp($timestamp);
}

/**
 * Gets list of matching articles by content type and sent filter.
 *
 * @param string $contentType
 *   Content type retrieved via GET.
 * @param string $rawFilter
 *   Url encoded filter retrieved via GET. Example: title ct 'great' and date gt
 *   '2012-04-23T18:25:43.511Z'.
 *
 * @return array
 *   List of article filters.
 *
 * @throws Exception
 *   When content type is not found or filter parsing fails.
 */
function clever_reach_get_article_filter($contentType, $rawFilter) {
  $schema = clever_reach_get_schema_by_type($contentType);

  $filterParser = new FilterParser();
  $filters = $filterParser->generateFilters(
        $contentType,
        NULL,
        urlencode($rawFilter)
    );

  $filterValidator = new Validator();
  $filterValidator->validateFilters($filters, $schema);

  $conditionMappings = clever_reach_drupal_condition_mapping();
  $fieldMappings = clever_reach_drupal_field_mapping();

  $filterBy = array();
  foreach ($filters as $filter) {
    if (array_key_exists($filter->getAttributeCode(), $fieldMappings)) {
      $fieldCode = $fieldMappings[$filter->getAttributeCode()];
    }
    else {
      $fieldCode = $filter->getAttributeCode();
    }

    $fieldValue = $filter->getAttributeValue();
    $condition = $conditionMappings[$filter->getCondition()];

    if ($date = DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z', $filter->getAttributeValue())) {
      $fieldValue = $date->getTimestamp();
    }

    $filterBy[] = array(
      'field' => $fieldCode,
      'value' => $fieldValue,
      'condition' => $condition,
    );
  }

  return $filterBy;
}

/**
 * Gets search result and applies all sent filters.
 *
 * @param string $contentType
 *   Content type retrieved via GET.
 * @param array $articleFilter
 *   Url encoded filter retrieved via GET. Example: title ct 'great' and date
 *     gt
 *   '2012-04-23T18:25:43.511Z'.
 *
 * @return CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SearchResult
 *   Result object, containing all information about matched articles.
 *
 * @throws Exception
 */
function clever_reach_get_search_result($contentType, array $articleFilter) {
  $articleSearch = new CleverReachArticleSearch();
  $searchResult = new SearchResult();

  foreach ($articleSearch->get($articleFilter) as $article) {
    // Set result for static schema defined in searchable item schema.
    $attributes = array(
      new UrlAttribute(
            'url',
            $articleSearch->getUrlByArticle($article)
      ),
      new AuthorAttribute(
            'author',
            $article->name
      ),
      new ImageAttribute(
            'mainImage',
            ''
      ),
      new HtmlAttribute(
            'articleHtml',
            $articleSearch->getContentByArticle($article)
      ),
      new EnumAttribute(
            'status',
            $article->status
      ),
      new DateAttribute(
            'changed',
            clever_reach_timestamp_to_date($article->changed)
      ),
      new EnumAttribute(
            'comment',
            $article->comment
      ),
      new EnumAttribute(
            'promote',
            $article->promote
      ),
      new EnumAttribute(
            'sticky',
            $article->sticky
      ),
      new TextAttribute(
            'log',
            $article->log
      ),
      new EnumAttribute(
            'language',
            $article->language
      ),
      new DateAttribute(
            'revision_timestamp',
            clever_reach_timestamp_to_date($article->revision_timestamp)
      ),
    );

    // Set result for dynamic schema defined in searchable item schema.
    $params = array('entity_type' => 'node', 'bundle' => $contentType);

    // Function field_read_fields in drupal 7.1 doesn't support filtering
    // by content type and bundle, so both field and instances must be loaded.
    $fields = field_read_fields();
    $instances = array_column(
        field_read_instances($params),
        NULL,
        'field_name'
    );

    foreach ($fields as $fieldName => $field) {
      if (!array_key_exists($fieldName, $instances)) {
        continue;
      }

      if (!$field = CleverReachArticleSearchField::getField($field)) {
        continue;
      }

      if (!$fieldValue = $field->getSearchResultValue($article)) {
        continue;
      }

      $attributes[] = $fieldValue;
    }

    $searchResult->addSearchResultItem(
        new SearchResultItem(
            $contentType,
            $article->nid,
            $article->title,
            clever_reach_timestamp_to_date($article->created),
            $attributes
        )
    );
  }

  return $searchResult;
}

/**
 * Get condition mappings.
 *
 * @return array
 *   List of CleverReach - Drupal search condition mappings.
 */
function clever_reach_drupal_condition_mapping() {
  return array(
    Conditions::EQUALS => '=',
    Conditions::NOT_EQUAL => '<>',
    Conditions::GREATER_THAN => '>',
    Conditions::LESS_THAN => '<',
    Conditions::LESS_EQUAL => '<=',
    Conditions::GREATER_EQUAL => '>=',
    Conditions::CONTAINS => 'CONTAINS',
  );
}

/**
 * Get field mappings.
 *
 * @return array
 *   List of CleverReach - Drupal field mappings.
 */
function clever_reach_drupal_field_mapping() {
  return array(
    'id' => 'nid',
    'itemCode' => 'type',
    'date' => 'created',
    'author' => 'uid',
  );
}
