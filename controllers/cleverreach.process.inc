<?php

/**
 * @file
 * Async process starter.
 */

use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\TaskExecution\Exceptions\ProcessStorageGetException;

/**
 * Callback for the cleverreach.process route.
 */
function clever_reach_start_process() {
  $params = drupal_get_query_parameters();

  if (empty($params['guid'])) {
    drupal_access_denied();
  }

  $guid = $params['guid'];

  try {
    $process = clever_reach_get_process($guid);

    if ($process === NULL) {
      throw new ProcessStorageGetException(
        "Process runner with guid {$guid} does not exist."
      );
    }

    $runner = unserialize($process['runner']);
    $runner->run();
  }
  catch (\Exception $e) {
    Logger::logError($e->getMessage(), 'Integration');
  }

  try {
    clever_reach_delete_process($guid);
  }
  catch (\Exception $e) {
    Logger::logError($e->getMessage(), 'Integration');
  }
}

/**
 * Gets process by GUID.
 *
 * @param string $guid
 *   Unique generated code stored in database.
 *
 * @return array|null
 *   Process fetched by guid, if found returns array, otherwise null.
 */
function clever_reach_get_process($guid) {
  $query = db_select(CleverReachAsyncProcessStarterService::TABLE_NAME)
    ->fields(CleverReachAsyncProcessStarterService::TABLE_NAME)
    ->condition('id', $guid);

  if (!$execute = $query->execute()) {
    return NULL;
  }

  $result = $execute->fetchAllAssoc('id', PDO::FETCH_ASSOC);
  return !empty($result) ? reset($result) : NULL;
}

/**
 * Deletes process by GUID.
 *
 * @param string $guid
 *   Unique generated code stored in database.
 */
function clever_reach_delete_process($guid) {
  $process = clever_reach_get_process($guid);

  if ($process === NULL) {
    Logger::logError("Could not delete process with guid $guid");
  }
  else {
    db_delete(CleverReachAsyncProcessStarterService::TABLE_NAME)->condition('id', $guid)->execute();
  }
}
