<?php

/**
 * @file
 * V1 Searchable items endpoint.
 */

use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchItem\SearchableItem;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchItem\SearchableItems;

/**
 * Gets list of supported searchable items from CleverReach system.
 *
 * All content types defined in Drupal are currently supported.
 */
function clever_reach_get_searchable_items_response() {
  $searchableItems = new SearchableItems();
  $contentTypes = node_type_get_types();

  foreach ($contentTypes as $contentType) {
    $searchableItems->addSearchableItem(
        new SearchableItem(
            $contentType->type,
            $contentType->name
        )
    );
  }

  drupal_json_output($searchableItems->toArray());
}
