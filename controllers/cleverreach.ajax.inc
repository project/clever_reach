<?php

/**
 * @file
 * Ajax call handler for CleverReach configuration page.
 */

use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\TaskExecution\QueueItem;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\BusinessLogic\Sync\InitialSyncTask;

/**
 * Wakeup endpoint. Return JSON encoded string.
 *
 * @see cleverreah.routing.inc
 */
function clever_reach_wakeup() {
  $result = array('status' => 'success');
  CleverReachTaskQueue::wakeup();
  drupal_json_output($result);
}

/**
 * Sync Retry endpoint. Return JSON encoded string.
 *
 * @see cleverreah.routing.inc
 */
function clever_reach_sync_retry() {
  $result = array('status' => 'success');

  try {
    CleverReachTaskQueue::enqueue(new InitialSyncTask(), TRUE);
  }
  catch (Exception $e) {
    Logger::logError("Error restarting sync: {$e->getMessage()}");
    $result = array('status' => 'failed');
  }

  drupal_json_output($result);
}

/**
 * Build first email indicator endpoint. Return JSON encoded string.
 *
 * @see cleverreah.routing.inc
 */
function clever_reach_build_first_email() {
  $result = array('status' => 'success');
  /** @var CleverReachConfigService $configService */
  $configService = ServiceRegister::getService(Configuration::CLASS_NAME);

  $configService->setIsFirstEmailBuilt(TRUE);
  drupal_json_output($result);
}

/**
 * Endpoint for checking initial sync status. Return JSON encoded string.
 *
 * @see cleverreah.routing.inc
 */
function clever_reach_check_import_status() {
  /** @var CleverReach\Infrastructure\TaskExecution\Queue $queueService */
  $queueService = ServiceRegister::getService(
        Queue::CLASS_NAME
    );

  /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $syncTaskQueueItem */
  $syncTaskQueueItem = $queueService->findLatestByType('InitialSyncTask');
  /** @var CleverReach\BusinessLogic\Sync\InitialSyncTask $syncTask */
  $syncTask = $syncTaskQueueItem->getTask();
  $syncProgress = $syncTask->getProgressByTask();

  if ($syncTaskQueueItem) {
    $result = array(
      'status' => $syncTaskQueueItem->getStatus(),
      'taskStatuses' => array(
        'subscriber_list' => array(
          'status' => clever_reach_get_initial_sync_status(
                    $syncProgress['subscriberList']
          ),
          'progress' => $syncProgress['subscriberList'],
        ),
        'add_fields' => array(
          'status' => clever_reach_get_initial_sync_status(
                    $syncProgress['fields']
          ),
          'progress' => $syncProgress['fields'],
        ),
        'recipient_sync' => array(
          'status' => clever_reach_get_initial_sync_status(
                    $syncProgress['recipients']
          ),
          'progress' => $syncProgress['recipients'],
        ),
      ),
      'syncProgress' => $syncProgress,
    );
  }
  else {
    $result = array('status' => QueueItem::FAILED);
  }

  drupal_json_output($result);
}

/**
 * Endpoint for checking initial sync status. Return JSON encoded string.
 *
 * @see cleverreah.routing.inc
 */
function clever_reach_check_auth_status() {
  $status = 'finished';
  /** @var CleverReach\Infrastructure\TaskExecution\Queue $queueService */
  $queueService = ServiceRegister::getService(Queue::CLASS_NAME);

  /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem */
  if ($queueItem = $queueService->findLatestByType('RefreshUserInfoTask')) {
    $queueStatus = $queueItem->getStatus();
    if (
        $queueStatus !== QueueItem::FAILED &&
        $queueStatus !== QueueItem::COMPLETED
    ) {
      $status = QueueItem::IN_PROGRESS;
    }
  }

  drupal_json_output(array('status' => $status));
}

/**
 * Get current status of initial sync based on provided progress.
 *
 * @param int $progress
 *   Current progress of initial sync execution.
 *
 * @see CleverReach\Infrastructure\TaskExecution\QueueItem
 *
 * @return string
 *   Current status.
 */
function clever_reach_get_initial_sync_status($progress) {
  $status = QueueItem::QUEUED;
  if (0 < $progress && $progress < 100) {
    $status = QueueItem::IN_PROGRESS;
  }
  elseif ($progress >= 100) {
    $status = QueueItem::COMPLETED;
  }
  return $status;
}
