<?php

/**
 * @file
 * Event handler endpoint.
 */

use CleverReach\BusinessLogic\Interfaces\Proxy as ProxyInterface;
use CleverReach\BusinessLogic\Sync\RecipientSyncTask;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Queue;

/**
 * Hook handle endpoint.
 *
 * Route cleverreach/hookhandler.
 *
 * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueStorageUnavailableException
 */
function cleverreach_handler() {
  if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    cleverreach_confirm_handler();
  }
  elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    cleverreach_process_event();
  }
}

/**
 * Route GET cleverreach/hookhandler.
 */
function cleverreach_confirm_handler() {
  $params = drupal_get_query_parameters();
  if ($params['secret']) {
    $verificationToken = cleverreach_get_config_service()
      ->getCrEventHandlerVerificationToken();
    drupal_add_http_header('status', '200 OK');
    die($verificationToken . ' ' . $params['secret']);
  }

  drupal_add_http_header('status', '400 Bad Request');
  die();
}

/**
 * Route POST cleverreach/hookhandler.
 *
 * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueStorageUnavailableException
 */
function cleverreach_process_event() {
  $crCallToken = $_SERVER['HTTP_X_CR_CALLTOKEN'];
  $configService = cleverreach_get_config_service();
  $content = file_get_contents('php://input');
  $body = json_decode($content, TRUE);

  if ($crCallToken === $configService->getCrEventHandlerCallToken()) {
    if (cleverreach_handle_event($body)) {
      cleverreach_set_subscription($body);
    }

    drupal_add_http_header('status', '200 OK');
  }
  else {
    drupal_add_http_header('status', '401 Unauthorized');
  }
}

/**
 * Subscribe or unsubscribe user, depending on the request.
 *
 * @param array $body
 *   JSON encoded request body.
 *
 * @throws QueueStorageUnavailableException
 * @throws \Exception
 */
function cleverreach_set_subscription(array $body) {
  $payload = $body['payload'];
  /** @var \CleverReach\BusinessLogic\Proxy $proxy */
  $proxy = ServiceRegister::getService(ProxyInterface::CLASS_NAME);

  $user = $proxy->getRecipient($payload['group_id'], $payload['pool_id']);
  /** @var \stdClass $targetUser */
  $targetUser = user_load_by_mail($user->getEmail());

  if ($targetUser) {
    if ($body['event'] === 'receiver.subscribed') {
      $targetUser->data['field_cleverreach_subscribed'] = 1;
    }
    elseif ($body['event'] === 'receiver.unsubscribed') {
      $targetUser->data['field_cleverreach_subscribed'] = 0;
    }

    user_save($targetUser);

    /** @var CleverReach\Infrastructure\TaskExecution\Queue $queue */
    $queue = ServiceRegister::getService(Queue::CLASS_NAME);
    $queue->enqueue(cleverreach_get_config_service()->getQueueName(),
      new RecipientSyncTask(array($targetUser->uid)));
  }
}

/**
 * Method that checks whether the event should be handled by this integration.
 *
 * @param array $body
 *   JSON body.
 *
 * @return bool
 *   Returns TRUE if event should be handled.
 */
function cleverreach_handle_event(array $body) {
  $integrationId = cleverreach_get_config_service()->getIntegrationId();
  $payload = $body['payload'];
  $allowedEvents = array('receiver.subscribed', 'receiver.unsubscribed');

  return module_exists('clever_reach')
    && $integrationId === $payload['group_id']
    && in_array($body['event'], $allowedEvents, TRUE);
}

/**
 * Returns configuration service.
 *
 * @return \CleverReachConfigService
 *   CleverReach configuration service.
 */
function cleverreach_get_config_service() {
  return ServiceRegister::getService(Configuration::CLASS_NAME);
}
