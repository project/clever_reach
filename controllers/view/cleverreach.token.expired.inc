<?php

use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\BusinessLogic\Interfaces\Proxy;

/**
 * Token Expired View Controller.
 *
 * @see template file cleverreach-token-expired.tpl.php
 */
class CleverReachTokenExpiredController extends CleverReachResolveStateController {

  const CURRENT_STATE_CODE = 'tokenexpired';

  const TEMPLATE = 'cleverreach_token_expired';

  /**
   * Executes POST request method.
   *
   * Prepares error screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function post(array &$form) {
    return $this->get($form);
  }

  /**
   * Executes GET request method.
   *
   * Prepares error screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    parent::get($form);
    $userInfo = $this->getConfigService()->getUserInfo();
    $translatableHello = t('hello');

    $result = array(
      '#urls' => array(
        'logo_url' => file_create_url(
          $this->getThemePath(
            'images/icon_' . $translatableHello . '.png'
          )
        ),
        'auth_url' => $this->getAuthUrl(),
        'check_status_url' => $this->getControllerUrl('auth.check.status'),
        'wakeup_url' => $this->getControllerUrl('wakeup'),
      ),
      '#config' => array(
        'client_id' => $userInfo['id'],
        'integration_name' => $this->getConfigService()->getIntegrationName(),
      ),
      '#theme' => self::TEMPLATE,
      '#attached' => array(
        'css' => array(
          $this->getThemePath('css/cleverreach.css'),
        ),
        'js' => array(
          $this->getThemePath('js/cleverreach.welcome.js'),
          $this->getThemePath('js/cleverreach.authorization.js'),
          $this->getThemePath('js/cleverreach.ajax.js'),
        ),
      ),
    );

    return $result;
  }

  /**
   * Gets auth URL with all register data.
   *
   * @return string
   *   Base 64 encoded array of new user parameters
   *   retrieved from configuration.
   */
  private function getAuthUrl() {
    global $language;
    /** @var CleverReach\BusinessLogic\Proxy $proxy */
    $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);

    return $proxy->getAuthUrl(
      $this->getControllerUrl(
        'callback',
        array(
          'refresh' => TRUE,
        )
      ),
      '',
      array('lang' => $language->language)
    );
  }

}
