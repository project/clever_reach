<?php

/**
 * Initial Sync View Controller.
 *
 * @see template file cleverreach-initial-sync.tpl.php
 */
class CleverReachInitialSyncController extends CleverReachResolveStateController {
  const CURRENT_STATE_CODE = 'initialsync';
  const TEMPLATE = 'cleverreach_initial_sync';

  /**
   * Executes GET request method.
   *
   * Prepares initial sync screen and list all available progress items.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    parent::get($form);

    return array(
      '#urls' => array(
        'check_status_url' => $this->getControllerUrl('import.check.status'),
      ),
      '#progress_items' => array(
        'subscriber_list' => t('Create recipient list in CleverReach®'),
        'add_fields' => t('Add data fields, segments and tags to recipient list'),
        'recipient_sync' => t('Import recipients from Drupal to CleverReach®'),
      ),
      '#theme' => self::TEMPLATE,
      '#attached' => array(
        'css' => array(
          $this->getThemePath('css/cleverreach.css'),
          $this->getThemePath('css/cleverreach-initial-sync.css'),
          $this->getThemePath('css/cleverreach-icofont.css'),
        ),
        'js' => array(
          $this->getThemePath('js/cleverreach.initial-sync.js'),
          $this->getThemePath('js/cleverreach.status-checker.js'),
          $this->getThemePath('js/cleverreach.ajax.js'),
        ),
      ),
    );
  }

}
