<?php

/**
 * Initial Sync View Controller.
 *
 * @see template file cleverreach-initial-sync.tpl.php
 */
class CleverReachInitialSyncConfigController extends CleverReachResolveStateController {
  const CURRENT_STATE_CODE = 'initialsync_config';
  const TEMPLATE = 'cleverreach_initial_sync_config';

  /**
   * Executes POST request method.
   *
   * Prepares configuration screen before initial sync.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function post(array &$form) {
    return $this->get($form);
  }

  /**
   * Executes GET request method.
   *
   * Prepares configuration screen before initial sync.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    parent::get($form);

    $userInfo = $this->getConfigService()->getUserInfo();

    return array(
      '#recipient_id' => $userInfo['id'],
      '#urls' => array(
        'logo_url' => file_create_url($this->getThemePath('images/icon_quickstartmailing.svg')),
        'configuration_url' => $this->getControllerUrl('configuration'),
      ),
      '#theme' => self::TEMPLATE,
      '#attached' => array(
        'css' => array(
          $this->getThemePath('css/cleverreach.css'),
          $this->getThemePath('css/cleverreach-dashboard.css'),
          $this->getThemePath('css/cleverreach-icofont.css'),
        ),
        'js' => array(
          $this->getThemePath('js/cleverreach.initial-sync-config.js'),
          $this->getThemePath('js/cleverreach.ajax.js'),
        ),
      ),
    );
  }

}
