<?php

use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\QueueItem;
use CleverReach\BusinessLogic\Utility\SingleSignOn\SingleSignOnProvider;

/**
 * Dashboard View Controller.
 *
 * @see template file cleverreach-dashboard.tpl.php
 */
class CleverReachDashboardController extends CleverReachResolveStateController {
  const CURRENT_STATE_CODE = 'dashboard';
  const TEMPLATE = 'cleverreach_dashboard';

  /**
   * Executes POST request method.
   *
   * Prepares data for dashboard screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function post(array &$form) {
    return $this->get($form);
  }

  /**
   * Executes GET request method.
   *
   * Prepares data for dashboard screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    parent::get($form);

    $userInfo = $this->getConfigService()->getUserInfo();
    $failureParameters = $this->getInitialSyncFailureParameters();
    $url = SingleSignOnProvider::getUrl(CleverReachConfigService::CLEVERREACH_BUILD_EMAIL_URL);

    $viewParams = array(
      '#recipient_id' => $userInfo['id'],
      '#integration_name' => $this->getConfigService()->getIntegrationName(),
      '#is_built_first_email' => $this->getConfigService()->isFirstEmailBuilt(),
      '#is_initial_sync_failed' => $failureParameters['is_failed'],
      '#initial_sync_failed_message' => $failureParameters['message'],
      '#urls' => array(
        'logo_url' => file_create_url($this->getThemePath('images/icon_quickstartmailing.svg')),
        'dashboard_logo_url' => file_create_url($this->getThemePath('images/cr_logo_transparent_107px.png')),
        'built_email_url' => $url,
        'built_first_email_url' => $this->getControllerUrl('build.first.email'),
        'retry_sync_url' => $this->getControllerUrl('retry.sync'),
      ),
    );

    if (!$failureParameters['is_failed']) {
      $viewParams['#report'] = $this->getInitialSyncReport();
    }

    $theme = array(
      '#theme' => self::TEMPLATE,
      '#attached' => array(
        'css' => array(
          $this->getThemePath('css/cleverreach-font-awesome.css'),
          $this->getThemePath('css/cleverreach.css'),
          $this->getThemePath('css/cleverreach-dashboard.css'),
        ),
        'js' => array(
          $this->getThemePath('js/cleverreach.dashboard.js'),
          $this->getThemePath('js/cleverreach.ajax.js'),
        ),
      ),
    );

    return array_merge($viewParams, $theme);
  }

  /**
   * Gets fail parameters for initial sync task if it failed.
   *
   * @return array
   *   Returns initial sync status array.
   *   Example: ['is_failed' => TRUE, 'message' => ''].
   */
  private function getInitialSyncFailureParameters() {
    $params = array('is_failed' => FALSE, 'message' => '');
    /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $initialSyncTask */
    $initialSyncTask = $this->getQueueService()->findLatestByType('InitialSyncTask');

    if (
      $initialSyncTask &&
      $initialSyncTask->getStatus() === QueueItem::FAILED
    ) {
      $params = array(
        'is_failed' => TRUE,
        'message' => $initialSyncTask->getFailureDescription(),
      );
    }

    return $params;
  }

  /**
   * Generates initial sync report.
   *
   * @return array
   *   Array with initial sync report.
   *
   * @throws \Exception
   */
  private function getInitialSyncReport() {
    $configService = $this->getConfigService();
    $result = array(
      'isReportEnabled' => !$configService->isImportStatisticsDisplayed(),
    );

    if ($result['isReportEnabled']) {
      global $language;
      $result['recipients'] = number_format(
        $configService->getNumberOfSyncedRecipients(),
        0,
        $language->language === 'en' ? '.' : ',',
        $language->language === 'en' ? ',' : '.'
      );
      $result['name'] = $configService->getIntegrationListName();
      $result['tags'] = $this->getFormattedTags();
      $configService->setImportStatisticsDisplayed(TRUE);
    }

    return $result;
  }

  /**
   * Generates formatted tags.
   *
   * @return array
   *   Array of formatted tags.
   */
  private function getFormattedTags() {
    $result = array();
    $recipientsService = ServiceRegister::getService(Recipients::CLASS_NAME);
    $tags = $recipientsService->getAllTags()->toArray();
    $numberOfTags = \count($tags);
    if ($numberOfTags === 0) {
      return $result;
    }

    $trimmedTags = array_slice($tags, 0, 3);
    /* @var int $index Foreach iterator. */
    /* @var \CleverReach\BusinessLogic\Entity\Tag $tag Tag entity. */
    foreach ($trimmedTags as $index => $tag) {
      $result[] = '<div class="value" title="' . $tag->getTitle() . '">'
                      . $this->getTrimmedTagName($index + 1 . ') ' . $tag->getTitle())
                . '</div>';
    }

    if ($numberOfTags > 3) {
      $result[] = '<div class="value">...</div>';
    }

    return $result;
  }

  /**
   * Trims tag name to specified length.
   *
   * @param string $tag
   *   Tag to get trimmed name.
   * @param int $maxChars
   *   Max length of trimmed name.
   * @param string $filler
   *   Trim filler.
   *
   * @return string
   *   Trimmed tag.
   */
  private function getTrimmedTagName($tag, $maxChars = 24, $filler = '...') {
    $length = \strlen($tag);
    $filterLength = \strlen($filler);

    return $length > $maxChars ? substr_replace(
      $tag,
      $filler,
      $maxChars - $filterLength,
      $length - $maxChars
    ) : $tag;
  }

}
