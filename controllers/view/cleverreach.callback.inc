<?php

use CleverReach\BusinessLogic\Entity\AuthInfo;
use CleverReach\BusinessLogic\Sync\RefreshUserInfoTask;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\BusinessLogic\Interfaces\Proxy;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Exceptions\BadAuthInfoException;

/**
 * Callback View Controller.
 *
 * @see template file cleverreach-welcome.tpl.php
 */
class CleverReachCallbackController extends CleverReachResolveStateController {

  const TEMPLATE = 'cleverreach_callback';

  /**
   * Executes GET request method.
   *
   * CleverReach auth callback controller.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    $params = drupal_get_query_parameters();

    if (empty($params['code'])) {
      drupal_set_message(t('Wrong parameters. Code not set.'));
    }
    else {

      if (isset($params['refresh'])) {
        $result = $this->getAuthInfo($params['code'], TRUE);
        $this->refreshTokens($result);
      }
      else {
        try {
          $result = $this->getAuthInfo($params['code'], FALSE);
          $this->queueRefreshUserTask($result);
        }
        catch (Exception $e) {
          drupal_set_message($e->getMessage());
        }
      }
    }

    return array('#theme' => self::TEMPLATE);
  }

  /**
   * Gets CleverReach auth info.
   *
   * @param string $code
   *   Code retrieved from CleverReach.
   * @param bool $refreshTokens
   *   Refresh tokens.
   *
   * @return \CleverReach\BusinessLogic\Entity\AuthInfo
   *   Auth info object.
   *
   * @throws \CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException
   */
  private function getAuthInfo($code, $refreshTokens) {
    /** @var CleverReach\BusinessLogic\Proxy $proxy */
    $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);

    try {
      /** @var \CleverReach\BusinessLogic\Entity\AuthInfo $result */
      $result = $proxy->getAuthInfo(
        $code,
        $this->getControllerUrl(
          'callback',
          $refreshTokens ? array('refresh' => TRUE) : array()
        )
      );
    }
    catch (BadAuthInfoException $e) {
      drupal_json_output(
        array(
          'status' => FALSE,
          'config' => $e->getMessage(),
        )
          );
    }

    return $result;
  }

  /**
   * Enqueues refresh user task.
   *
   * @param \CleverReach\BusinessLogic\Entity\AuthInfo $authInfo
   *   CleverReach auth info object.
   *
   * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueStorageUnavailableException
   * @throws \Exception
   */
  private function queueRefreshUserTask(AuthInfo $authInfo) {
    /** @var CleverReach\Infrastructure\TaskExecution\Queue $queue */
    $queue = ServiceRegister::getService(Queue::CLASS_NAME);
    /** @var \CleverReachConfigService $configService */
    $configService = ServiceRegister::getService(Configuration::CLASS_NAME);
    $configService->setAccessToken($authInfo->getAccessToken());
    $queue->enqueue(
      $configService->getQueueName(),
      new RefreshUserInfoTask($authInfo)
    );
  }

  /**
   * Refreshes user access tokens.
   *
   * @param \CleverReach\BusinessLogic\Entity\AuthInfo $authInfo
   *   User auth info object.
   *
   * @throws \CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException
   * @throws \CleverReach\Infrastructure\Exceptions\InvalidConfigurationException
   * @throws \CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException
   * @throws \CleverReach\Infrastructure\Utility\Exceptions\RefreshTokenExpiredException
   * @throws \Exception
   *
   * @since 1.1.0
   */
  private function refreshTokens(AuthInfo $authInfo) {
    /** @var \CleverReachConfigService $configService */
    $configService = ServiceRegister::getService(Configuration::CLASS_NAME);
    /** @var \CleverReach\BusinessLogic\Proxy $proxy */
    $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);

    $userInfo = $proxy->getUserInfo($authInfo->getAccessToken());
    $localInfo = $configService->getUserInfo();

    if (isset($userInfo['id']) && $userInfo['id'] === $localInfo['id']) {
      $configService->setAccessToken($authInfo->getAccessToken());
      $configService->setRefreshToken($authInfo->getRefreshToken());
      $configService->setAccessTokenExpirationTime($authInfo->getAccessTokenDuration());
    }
  }

}
