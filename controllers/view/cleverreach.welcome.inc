<?php

use CleverReach\BusinessLogic\Interfaces\Proxy;
use CleverReach\Infrastructure\ServiceRegister;

/**
 * Welcome View Controller.
 *
 * @see template file cleverreach-welcome.tpl.php
 */
class CleverReachWelcomeController extends CleverReachResolveStateController {
  const CURRENT_STATE_CODE = 'welcome';
  const TEMPLATE = 'cleverreach_welcome';

  /**
   * Executes POST request method.
   *
   * Prepares authentication screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function post(array &$form) {
    return $this->get($form);
  }

  /**
   * Executes GET request method.
   *
   * Prepares authentication screen.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    parent::get($form);
    $translatableHello = t('hello');

    $result = array(
      '#urls' => array(
        'logo_url' => file_create_url(
          $this->getThemePath(
            'images/icon_' . $translatableHello . '.png'
          )
        ),
        'auth_url' => $this->getAuthUrl(),
        'check_status_url' => $this->getControllerUrl('auth.check.status'),
        'wakeup_url' => $this->getControllerUrl('wakeup'),
      ),
      '#theme' => self::TEMPLATE,
      '#attached' => array(
        'css' => array(
          $this->getThemePath('css/cleverreach.css'),
        ),
        'js' => array(
          $this->getThemePath('js/cleverreach.welcome.js'),
          $this->getThemePath('js/cleverreach.authorization.js'),
          $this->getThemePath('js/cleverreach.ajax.js'),
        ),
      ),
    );

    return $result;
  }

  /**
   * Gets auth URL with all register data.
   *
   * @return string
   *   Base 64 encoded array of new user parameters
   *   retrieved from configuration.
   */
  private function getAuthUrl() {
    global $language;
    /** @var CleverReach\BusinessLogic\Proxy $proxy */
    $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
    $registerData = base64_encode(json_encode($this->getRegisterData()));

    return $proxy->getAuthUrl(
      $this->getControllerUrl('callback'),
      $registerData,
      array('lang' => $language->language)
    );
  }

  /**
   * Gets admin user registration data for CleverReach.
   *
   * @return array
   *   List of available parameters for current user.
   */
  private function getRegisterData() {
    global $user;
    $username = $user->name;
    $siteName = $this->getConfigService()->getSiteName();
    $email = variable_get('site_mail', $user->mail);

    return array(
      'email' => $email,
      'company' => $siteName,
      'firstname' => $username,
      'lastname' => '',
      'gender' => '',
      'street' => '',
      'zip' => '',
      'city' => '',
      'country' => '',
      'phone' => '',
    );
  }

}
