<?php

use CleverReach\BusinessLogic\Sync\InitialSyncTask;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\TaskExecution\QueueItem;

/**
 * Resolver Controller. Resolves current state of user synchronization.
 */
class CleverReachResolveStateController extends CleverReachBaseController {

  const TEMPLATE = '';

  const CURRENT_STATE_CODE = '';

  const WELCOME_STATE_CODE = 'welcome';

  const INITIAL_SYNC_CONFIG_STATE_CODE = 'initialsync_config';

  const INITIAL_SYNC_STATE_CODE = 'initialsync';

  const TOKEN_EXPIRED_STATE_CODE = 'tokenexpired';

  const DASHBOARD_STATE_CODE = 'dashboard';

  /**
   * Instance of Queue class.
   *
   * @var CleverReach\Infrastructure\TaskExecution\Queue
   */
  private $queue;

  /**
   * Instance of Configuration class.
   *
   * @var CleverReachConfigService
   */
  private $configService;

  /**
   * Executes GET request method.
   *
   * @param array $form
   *   A keyed array containing the current state of the form.
   *
   * @return array
   *   Array of template vars.
   *
   * @throws Exception
   */
  public function get(array &$form) {
    CleverReachTaskQueue::wakeup();

    if (!$this->isAuthTokenValid()) {
      return $this->redirect(self::WELCOME_STATE_CODE);
    }

    if (!$this->isSyncConfigured()) {
      return $this->redirect(self::INITIAL_SYNC_CONFIG_STATE_CODE);
    }

    if ($this->isInitialSyncInProgress()) {
      return $this->redirect(self::INITIAL_SYNC_STATE_CODE);
    }

    if (!$this->checkIfTokenIsRefreshed()) {
      return $this->redirect(self::TOKEN_EXPIRED_STATE_CODE);
    }

    return $this->redirect(self::DASHBOARD_STATE_CODE);
  }

  /**
   * Gets resource path.
   *
   * @param string $resourcePath
   *   Resource path relative to theme folder under cleverreach module.
   * @param bool $absolute
   *   When true, absolute path is returned.
   *
   * @return string
   *   Base path to provided resource.
   */
  protected function getThemePath($resourcePath, $absolute = FALSE) {
    $path = drupal_get_path('module', 'clever_reach') . "/theme/$resourcePath";

    if ($absolute) {
      $path = "/$path";
    }

    return $path;
  }

  /**
   * Gets full url to the controller by provided route name.
   *
   * @param string $name
   *   CleverReach Route name.
   * @param array $params
   *   Query parameters, in the form of key-value pairs.
   *
   * @return string
   *   Full url to controller.
   *
   * @see cleverreach.routing.php
   */
  protected function getControllerUrl($name, array $params = array()) {
    return url(clever_reach_get_route_by_name($name), array(
      'absolute' => TRUE,
      'query' => $params,
    ));
  }

  /**
   * Gets CleverReach configuration service.
   *
   * @return CleverReachConfigService
   *   New instance of ConfigService.
   */
  protected function getConfigService() {
    if (NULL === $this->configService) {
      $this->configService = ServiceRegister::getService(
        Configuration::CLASS_NAME
      );
    }

    return $this->configService;
  }

  /**
   * Gets CleverReach queue service.
   *
   * @return CleverReach\Infrastructure\TaskExecution\Queue
   *   New instance of Queue.
   */
  protected function getQueueService() {
    if (NULL === $this->queue) {
      $this->queue = ServiceRegister::getService(Queue::CLASS_NAME);
    }

    return $this->queue;
  }

  /**
   * Redirects user to proper state.
   *
   * @param string $routeName
   *   CleverReach Route name.
   */
  private function redirect($routeName) {
    if (static::CURRENT_STATE_CODE !== $routeName) {
      drupal_goto($this->getControllerUrl($routeName));
    }
  }

  /**
   * Checks if user is logged in to CleverReach account.
   *
   * @return bool
   *   If auth token is valid returns true, otherwise false.
   */
  private function isAuthTokenValid() {
    $accessToken = $this->getConfigService()->getAccessToken();
    return !empty($accessToken);
  }

  /**
   * Checks if user is already configured initial sync.
   *
   * @return bool
   *   If sync configured returns true, otherwise false.
   */
  private function isSyncConfigured() {
    return $this->getConfigService()->isConfiguredInitialSync();
  }

  /**
   * Checks if initial sync is in progress.
   *
   * @return bool
   *   If initial sync is in progress returns true, otherwise false.
   */
  private function isInitialSyncInProgress() {
    /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $initialSyncTask */
    if (!$initialSyncTask = $this->getQueueService()
      ->findLatestByType('InitialSyncTask')) {
      try {
        CleverReachTaskQueue::enqueue(new InitialSyncTask());
      }
      catch (Exception $e) {
        // If task enqueue fails do nothing but report
        // that initial sync is in progress.
      }

      return TRUE;
    }

    return !in_array($initialSyncTask->getStatus(), array(
      QueueItem::COMPLETED,
      QueueItem::FAILED,
    ), TRUE);
  }

  /**
   * Checks if token has been refreshed.
   *
   * @return bool
   *   TRUE if token is refreshed.
   */
  private function checkIfTokenIsRefreshed() {
    $refreshToken = $this->getConfigService()->getRefreshToken();
    $exchangeTokenTask = $this->getQueueService()->findLatestByType('ExchangeAccessTokenTask');
    return !empty($refreshToken)
      || ($exchangeTokenTask !== NULL
        && !in_array($exchangeTokenTask->getStatus(), array(QueueItem::COMPLETED, QueueItem::FAILED), TRUE)
      );
  }

}
