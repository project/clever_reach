CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Features
 * Support

INTRODUCTION
------------

CleverReach® - Newsletter Marketing

CleverReach® was founded in 2007 and is one of the leading providers for email
marketing, with more than 190,000 customers in 152 countries. A big plus of the
German company based in Rastede, apart from the user-friendly menu navigation of
the software, is the competent customer service and the very fair
price-performance ratio. CleverReach® also meets the highest data protection
standards, exceeding legal requirements.

Thanks to the sophisticated technology, the app is set up within a few minutes
and ready for your first mailing.

REQUIREMENTS
------------
This module requires the following modules:

 * User (https://www.drupal.org/docs/7/core/modules/user)
 * Field (https://www.drupal.org/docs/7/core/modules/field)

INSTALLATION
------------
This module requires the following modules:

 * Download CleverReach® module from:
   https://www.drupal.org/project/clever_reach
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Once module installation is finished, navigate to:
  "Configuration" -> "CleverReach®" Configuration -> CleverReach®

CONFIGURATION
------------
 * Once module installation is finished, navigate to:
  "Configuration" -> "CleverReach®" Configuration -> CleverReach®
 * Create a new account or log in with your existing data.
 * Configure how to synchronize existing Drupal users. If this option is set,
   all users that existed before CleverReach® initial synchronization will be
   synced as Active. CleverReach® module add newsletter subscription status
   so you can later set this per user.

FEATURES
------------
Overview of the key features:

 * Easily create and send email newsletters with the drag & drop editor
 * Responsive, free templates for various industries and occasions
 * Analyze success and optimize your newsletters
 * Highest security standards, Email Marketing made in Germany
 * Free online and phone support & online seminars
 * The right pricing for everyone: customized, changeable at any time, no
   contract term - Scales for small and big contact lists: prepaid for
   occasional senders, flat rate for regular senders or high volume for
   big players

Lots of additional features: create newsletters, newsletter editor,
occasion-related newsletter templates, A/B Split tests, spam tests, Image
processing, RSS-Feeds, dynamic content, Import functions, Segmentation of
recipients lists, Double-Opt-In registration forms, automatic bounce management,
automatic unsubscribe management, Blacklist Check, send newsletters,
personalized newsletter delivery, Social Media Integration, Reporting &
Tracking, open and click rate, unsubscribe rate, unsubscribes, multi-level
bounce management, Google Analytics Integration, Conversion Tracking,
automated mailings, Lifecycle Email Marketing, THEA - The Email Marketing
Automation, Autoresponder, Follow-Ups, Whitelisting, CSA, Certified Senders
Alliance, Design tests, Newsletter Client Testing SPF, Senders Policy Framework,
Email Authentication, SSL encoding, Wordpress Plugin, direct mailing,
Import of CSV files, responsive templates, free software, Typo3 Newsletter,
import of excel files, forms, direct marketing and many other features!

SUPPORT
------------
https://support.cleverreach.de/hc/en-us/requests/new
