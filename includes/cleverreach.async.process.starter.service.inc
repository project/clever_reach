<?php

use CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\Interfaces\Required\HttpClient;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Exceptions\ProcessStarterSaveException;
use CleverReach\Infrastructure\Utility\GuidProvider;
use CleverReach\Infrastructure\Interfaces\Required\AsyncProcessStarter;
use CleverReach\Infrastructure\Interfaces\Exposed\Runnable;

/**
 * Implementation of async process starter interface.
 *
 * @see \CleverReach\Infrastructure\Interfaces\Required\AsyncProcessStarter
 */
class CleverReachAsyncProcessStarterService implements AsyncProcessStarter {
  const TABLE_NAME = 'cleverreach_process';
  const XDEBUG_KEY = '';

  /**
   * Starts given runner asynchronously (in new process/web request or similar)
   *
   * @param \CleverReach\Infrastructure\Interfaces\Exposed\Runnable $runner
   *   Runner that should be started async.
   *
   * @throws HttpRequestException
   * @throws ProcessStarterSaveException
   */
  public function start(Runnable $runner) {
    $guidProvider = new GuidProvider();
    $guid = trim($guidProvider->generateGuid());

    $this->saveGuidAndRunner($guid, $runner);
    $this->startRunnerAsynchronously($guid);
  }

  /**
   * Saves runner and guid to storage.
   *
   * @param string $guid
   *   Unique request ID.
   * @param \CleverReach\Infrastructure\Interfaces\Exposed\Runnable $runner
   *   Runner that should be started async.
   *
   * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\ProcessStarterSaveException
   */
  private function saveGuidAndRunner($guid, Runnable $runner) {
    try {
      db_insert(self::TABLE_NAME)->fields(
        array(
          'id' => $guid,
          'runner' => serialize($runner),
        )
      )->execute();
    }
    catch (Exception $e) {
      Logger::logError($e->getMessage(), 'Integration');
      throw new ProcessStarterSaveException(
        $e->getMessage(),
        0,
        $e
        );
    }
  }

  /**
   * Starts runnable asynchronously.
   *
   * @param string $guid
   *   Unique request ID.
   *
   * @throws \CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException
   */
  private function startRunnerAsynchronously($guid) {
    try {
      /** @var CleverReachHttpClientService $httpService */
      $httpService = ServiceRegister::getService(
        HttpClient::CLASS_NAME
      );

      $httpService->requestAsync('POST', $this->formatUrl($guid));
    }
    catch (Exception $e) {
      Logger::logError($e->getMessage(), 'Integration');
      throw new HttpRequestException($e->getMessage(), 0, $e);
    }
  }

  /**
   * Gets process URL with all included parameters.
   *
   * @param int $guid
   *   Unique request ID.
   *
   * @return string
   *   Url to process controller.
   */
  private function formatUrl($guid) {
    $params = array('action' => 'run', 'guid' => $guid);

    if (self::XDEBUG_KEY) {
      $params['XDEBUG_SESSION_START'] = self::XDEBUG_KEY;
    }

    return url(
        clever_reach_get_route_by_name('process'),
        array('absolute' => TRUE, 'query' => $params)
    );
  }

}
