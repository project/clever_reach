<?php

use CleverReach\Infrastructure\Logger\LogData;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\Interfaces\LoggerAdapter;
use CleverReach\Infrastructure\Logger\Configuration;

/**
 * Logger service implementation.
 *
 * @see \CleverReach\Infrastructure\Interfaces\LoggerAdapter
 */
class CleverReachLoggerService implements LoggerAdapter {

  /**
   * Log message in system.
   *
   * @inheritdoc
   */
  public function logMessage($data) {
    $minLogLevel = Configuration::getInstance()->getMinLogLevel();
    $logLevel = $data->getLogLevel();

    // Min log level is actually max log level.
    if ($logLevel > $minLogLevel) {
      return;
    }

    $level = WATCHDOG_INFO;
    switch ($logLevel) {
      case Logger::ERROR:
        $level = WATCHDOG_ERROR;
        break;

      case Logger::WARNING:
        $level = WATCHDOG_WARNING;
        break;

      case Logger::DEBUG:
        $level = WATCHDOG_DEBUG;
        break;
    }

    watchdog(
        'cleverreach',
        $this->getMessage($data, $level),
        array(),
        $level
    );
  }

  /**
   * Gets formatted CleverReach log message.
   *
   * @param \CleverReach\Infrastructure\Logger\LogData $data
   *   Log data object.
   * @param string $level
   *   Drupal log level.
   *
   * @return string
   *   Full message data, containing all log info.
   */
  private function getMessage(LogData $data, $level) {
    $message = "[{$level}]";
    $message .= "[{$data->getTimestamp()}]";
    $message .= "[{$data->getComponent()}]";
    $message .= "[{$data->getUserAccount()}]";
    $message .= (string) $data->getMessage();
    return $message;
  }

}
