<?php

use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\ComplexCollectionAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\TextAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\HtmlAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\ObjectAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\ComplexCollectionSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\ObjectSchemaAttribute;

/**
 * CleverReach base schema field class.
 */
class CleverReachTextWithSummaryType extends CleverReachBaseField {

  /**
   * Get schema field for this type.
   *
   * @inheritdoc
   */
  public function getSchemaField() {
    if ($this->isSingleValue()) {
      return new ObjectSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        $this->getAttributes()
      );
    }

    return new ComplexCollectionSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        $this->getAttributes()
    );
  }

  /**
   * Get search result for this type.
   *
   * @inheritdoc
   */
  public function getSearchResultValue($node) {
    $attributes = array();
    $code = $this->getName();

    foreach ($this->getValue($node) as $value) {
      $attributes[] = new ObjectAttribute(
        $code, array(
          new HtmlAttribute(
                'value',
                $value['value']
          ),
          new TextAttribute(
                'summary',
                $value['summary']
          ),
        )
      );
    }

    if ($this->isSingleValue()) {
      if (empty($attributes)) {
        return new ObjectAttribute($code);
      }

      return $attributes[0];
    }

    return new ComplexCollectionAttribute(
        $code,
        $attributes
    );
  }

  /**
   * Gets searchable conditions for this type.
   *
   * @inheritdoc
   */
  protected function getSearchableConditions() {
    return array(
      Conditions::EQUALS,
      Conditions::NOT_EQUAL,
      Conditions::CONTAINS,
    );
  }

  /**
   * Indicates whether this type is searchable or not.
   *
   * @inheritdoc
   */
  protected function isSearchable() {
    return TRUE;
  }

  /**
   * Gets sub-attributes of data type.
   *
   * @return array
   *   List of sub attributes.
   */
  private function getAttributes() {
    return array(
      new SimpleSchemaAttribute(
            'value',
            t('Value'),
            FALSE,
            array(),
            SchemaAttributeTypes::HTML
      ),
      new SimpleSchemaAttribute(
            'summary',
            t('Summary'),
            FALSE,
            array(),
            SchemaAttributeTypes::TEXT
      ),
    );
  }

}
