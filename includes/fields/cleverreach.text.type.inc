<?php

use CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SimpleCollectionAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\TextAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleCollectionSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleSchemaAttribute;

/**
 * CleverReach base schema field class.
 */
class CleverReachTextType extends CleverReachBaseField {

  /**
   * Get schema field for this type.
   *
   * @inheritdoc
   */
  public function getSchemaField() {
    if ($this->isSingleValue()) {
      return new SimpleSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        SchemaAttributeTypes::TEXT
      );
    }

    return new SimpleCollectionSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        array(Conditions::CONTAINS),
        SchemaAttributeTypes::TEXT
    );
  }

  /**
   * Get search result for this type.
   *
   * @inheritdoc
   */
  public function getSearchResultValue($node) {
    $attributes = array();
    $code = $this->getName();

    foreach ($this->getValue($node) as $value) {
      if (!array_key_exists('value', $value)) {
        continue;
      }

      $attributes[] = new TextAttribute(
        $code,
        $value['value']
      );
    }

    if ($this->isSingleValue()) {
      if (empty($attributes)) {
        return new TextAttribute($code, '');
      }

      return $attributes[0];
    }

    return new SimpleCollectionAttribute(
        $code,
        $attributes
    );
  }

  /**
   * Gets searchable conditions for this type.
   *
   * @inheritdoc
   */
  protected function getSearchableConditions() {
    return array(
      Conditions::EQUALS,
      Conditions::NOT_EQUAL,
      Conditions::CONTAINS,
    );
  }

  /**
   * Indicates whether this type is searchable or not.
   *
   * @inheritdoc
   */
  protected function isSearchable() {
    return TRUE;
  }

}
