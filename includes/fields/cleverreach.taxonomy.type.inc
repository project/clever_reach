<?php

use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SimpleCollectionAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\TextAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleCollectionSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleSchemaAttribute;

/**
 * CleverReach base schema field class.
 */
class CleverReachTaxonomyType extends CleverReachBaseField {

  /**
   * Get schema field for this type.
   *
   * @inheritdoc
   */
  public function getSchemaField() {
    if ($this->isSingleValue()) {
      return new SimpleSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        SchemaAttributeTypes::TEXT
      );
    }

    return new SimpleCollectionSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        SchemaAttributeTypes::TEXT
    );
  }

  /**
   * Get search result for this type.
   *
   * @inheritdoc
   */
  public function getSearchResultValue($node) {
    $attributes = array();
    $code = $this->getName();

    foreach ($this->getValue($node) as $value) {
      if (!array_key_exists('tid', $value)) {
        continue;
      }

      if (!$term = taxonomy_term_load($value['tid'])) {
        continue;
      }

      $attributes[] = new TextAttribute(
        $code,
        $term->name
      );
    }

    if ($this->isSingleValue()) {
      if (empty($attributes)) {
        return new TextAttribute($code, '');
      }

      return $attributes[0];
    }

    return new SimpleCollectionAttribute(
        $code,
        $attributes
    );
  }

}
