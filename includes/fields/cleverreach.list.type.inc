<?php

use CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SimpleCollectionAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\EnumAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleCollectionSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\EnumSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\Enum;

/**
 * CleverReach base schema field class.
 */
class CleverReachListType extends CleverReachBaseField {

  /**
   * Get schema field for this type.
   *
   * @inheritdoc
   */
  public function getSchemaField() {
    $possibleValues = array();
    $options = empty($this->field['settings']['allowed_values']) ? array() :
            $this->field['settings']['allowed_values'];

    foreach ($options as $key => $label) {
      $possibleValues[] = new Enum($label, $key);
    }

    if ($this->isSingleValue()) {
      return new EnumSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        $possibleValues
      );
    }

    return new SimpleCollectionSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        array(Conditions::CONTAINS),
        SchemaAttributeTypes::ENUM
    );
  }

  /**
   * Get search result for this type.
   *
   * @inheritdoc
   */
  public function getSearchResultValue($node) {
    $attributes = array();
    $code = $this->getName();

    foreach ($this->getValue($node) as $value) {
      if (!array_key_exists('value', $value)) {
        continue;
      }

      $attributes[] = new EnumAttribute(
        $code,
        $value['value']
      );
    }

    if ($this->isSingleValue()) {
      if (empty($attributes)) {
        return new EnumAttribute($code, '');
      }

      return $attributes[0];
    }

    return new SimpleCollectionAttribute(
        $code,
        $attributes
    );
  }

  /**
   * Gets searchable conditions for this type.
   *
   * @inheritdoc
   */
  protected function getSearchableConditions() {
    return array(
      Conditions::EQUALS,
      Conditions::NOT_EQUAL,
    );
  }

  /**
   * Indicates whether this type is searchable or not.
   *
   * @inheritdoc
   */
  protected function isSearchable() {
    return TRUE;
  }

}
