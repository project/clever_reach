<?php

/**
 * CleverReach base schema field class.
 */
abstract class CleverReachBaseField {
  /**
   * Current field.
   *
   * @var array
   */
  protected $field;

  /**
   * BaseField constructor.
   *
   * @param array $field
   *   Field setter.
   */
  public function __construct(array $field) {
    $this->field = $field;
  }

  /**
   * Gets schema field converted to CleverReach SchemaAttribute.
   *
   * @return \CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttribute
   *   CleverReach schema attribute.
   */
  abstract public function getSchemaField();

  /**
   * Converts to search result object from Drupal's object base on type.
   *
   * @param object $node
   *   Drupal content type object.
   *
   * @return \CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\SimpleAttribute
   *   CleverReach search result attribute.
   */
  abstract public function getSearchResultValue($node);

  /**
   * Checks if passed field is single or multi value.
   *
   * @return bool
   *   Returns true if field is single value, otherwise false.
   */
  protected function isSingleValue() {
    return (int) $this->field['cardinality'] === 1;
  }

  /**
   * Returns unique machine code of the field.
   *
   * @return string
   *   Field name.
   */
  protected function getName() {
    return isset($this->field['field_name']) ? $this->field['field_name'] : '';
  }

  /**
   * Returns field label.
   *
   * @return string
   *   Label of the field.
   */
  protected function getLabel() {
    return isset($this->field['instance']['label']) ?
        $this->field['instance']['label'] : '';
  }

  /**
   * Gets value for specific node.
   *
   * @param object $node
   *   Content object.
   *
   * @return array
   *   List of values for field.
   */
  protected function getValue($node) {
    $code = $this->getName();
    return isset($node->{$code}[LANGUAGE_NONE]) ?
        $node->{$code}[LANGUAGE_NONE] : array();
  }

  /**
   * Gets list of searchable conditions.
   *
   * @see \CleverReach\BusinessLogic\Utility\ArticleSearch\Conditions
   *
   * @return array
   *   Gets supported searchable conditions.
   */
  protected function getSearchableConditions() {
    return array();
  }

  /**
   * Indicates whether field is searchable or not.
   *
   * @return bool
   *   If field searchable, return true, otherwise false.
   */
  protected function isSearchable() {
    return FALSE;
  }

}
