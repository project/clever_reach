<?php

use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SchemaAttributeTypes;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\SimpleSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\ComplexCollectionAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\TextAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\UrlAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\SearchResult\ObjectAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\ComplexCollectionSchemaAttribute;
use CleverReach\BusinessLogic\Utility\ArticleSearch\Schema\ObjectSchemaAttribute;

/**
 * CleverReach image type support.
 */
class CleverReachImageType extends CleverReachBaseField {

  /**
   * Get schema field for this type.
   *
   * @inheritdoc
   */
  public function getSchemaField() {
    if ($this->isSingleValue()) {
      return new ObjectSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        $this->getAttributes()
      );
    }

    return new ComplexCollectionSchemaAttribute(
        $this->getName(),
        $this->getLabel(),
        $this->isSearchable(),
        $this->getSearchableConditions(),
        $this->getAttributes()
    );
  }

  /**
   * Get search result for this type.
   *
   * @inheritdoc
   */
  public function getSearchResultValue($node) {
    $attributes = array();
    $code = $this->getName();

    foreach ($this->getValue($node) as $value) {
      if (!array_key_exists('uri', $value)) {
        continue;
      }

      $attributes[] = new ObjectAttribute(
        $code, array(
          new UrlAttribute(
                'url',
                file_create_url($value['uri'])
          ),
          new TextAttribute(
                'alt', $value['alt']
          ),
          new TextAttribute(
                'title',
                $value['title']
          ),
        )
      );
    }

    if ($this->isSingleValue()) {
      if (empty($attributes)) {
        return new ObjectAttribute($code);
      }

      return $attributes[0];
    }

    return new ComplexCollectionAttribute(
        $code,
        $attributes
    );
  }

  /**
   * Gets sub attributes of main image type.
   *
   * @return array
   *   List of sub attributes.
   */
  private function getAttributes() {
    return array(
      new SimpleSchemaAttribute(
            'url',
            t('URI'),
            FALSE,
            array(),
            SchemaAttributeTypes::URL
      ),
      new SimpleSchemaAttribute(
            'alt',
            t('Alt'),
            FALSE,
            array(),
            SchemaAttributeTypes::TEXT
      ),
      new SimpleSchemaAttribute(
            'title',
            t('Title'),
            FALSE,
            array(),
            SchemaAttributeTypes::TEXT
      ),
    );
  }

}
