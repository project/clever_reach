<?php

/**
 * Implementation of article search repository.
 */
class CleverReachArticleSearch {
  /**
   * List of entity (node) fields.
   *
   * @var array
   */
  private static $entityFields = array(
    'entity_type',
    'bundle',
    'revision_id',
    'entity_id',
  );

  /**
   * List of entity specific (node) properties.
   *
   * @var array
   * @see https://www.drupal.org/node/49768
   */
  private static $nodeProperties = array(
    'nid',
    'uid',
    'vid',
    'type',
    'language',
    'title',
    'uid',
    'status',
    'created',
    'changed',
    'comment',
    'promote',
    'sticky',
    'translate',
    'revision_uid',
    'revision_timestamp',
    'log',
    'name',
  );

  /**
   * Gets articles by provided filters.
   *
   * If filters are not set, all nodes (articles) will be returned.
   *
   * @param array|null $filterBy
   *   Example: ['field' => 'test', 'condition' => '=', 'value' =>
   *     'something'].
   *
   * @return stdClass[]
   *   List of matching nodes.
   */
  public function get($filterBy = NULL) {
    // Possible values for third parameter are list below:
    // - '=', '<>', '>', '>=', '<', '<=', 'STARTS_WITH', 'CONTAINS',
    //   'ENDS_WITH': These operators expect $value to be a literal of the
    //   same type as the column.
    // - 'IN', 'NOT IN': These operators expect $value to be an array of
    //   literals of the same type as the column.
    // - 'BETWEEN': This operator expects $value to be an array of two literals
    //   of the same type as the column.
    //
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');

    if (is_array($filterBy) && !empty($filterBy)) {
      foreach ($filterBy as $filter) {
        $condition = isset($filter['condition']) ? $filter['condition'] : '=';
        $value = $condition === 'CONTAINS' ?
                    db_like($filter['value']) : $filter['value'];

        if ($filter['field'] === 'uid') {
          $user = user_load_by_name($value);
          $value = $user ? $user->uid : $value;
        }

        if (in_array($filter['field'], self::$entityFields, TRUE)) {
          $query->entityCondition(
            $filter['field'],
            $value,
            $condition
          );
          continue;
        }

        if (in_array($filter['field'], self::$nodeProperties, TRUE)) {
          $query->propertyCondition(
            $filter['field'],
            $value,
            $condition
          );
          continue;
        }

        if ($condition === '<>') {
          if ($notInValue = $this->getNotEqualValue($filter)) {
            $query->propertyCondition('nid', $notInValue, 'NOT IN');
          }
          continue;
        }

        $query->fieldCondition(
            $filter['field'],
            'value',
            $value,
            $condition
        );
      }
    }

    if (!$result = $query->execute()) {
      return array();
    }

    return node_load_multiple(array_keys($result['node']));
  }

  /**
   * Gets URL by node.
   *
   * @param object $node
   *   Node object.
   * @param string|null $language
   *   (optional) Language code. If null default language is used.
   *
   * @return string
   *   Article/Node url.
   */
  public function getUrlByArticle($node, $language = NULL) {
    return url(
        "node/{$node->nid}",
        array('absolute' => TRUE, 'language' => $language)
    );
  }

  /**
   * Gets html content of element.
   *
   * @param object $node
   *   Node object.
   * @param string|null $language
   *   (optional) Language code. If null default language is used.
   *
   * @return string
   *   HTML of article.
   */
  public function getContentByArticle($node, $language = NULL) {
    $node->in_preview = TRUE;
    $view = node_view($node, 'full', $language);

    if (!$html = drupal_render($view)) {
      return '';
    }

    $doc = new DOMDocument();
    libxml_use_internal_errors(TRUE);
    $doc->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
    $this->convertToAbsoluteUrls($doc, 'img', 'src');
    $this->convertToAbsoluteUrls($doc, 'a', 'href');

    return $doc->saveHTML($doc);
  }

  /**
   * Not equal condition handler.
   *
   * Drupal 7 doesn't support left/right join for entity field query,
   * additional query must be created in order to support neq condition.
   *
   * @param array $filter
   *   Not equals filter array.
   *
   * @see https://www.drupal.org/project/drupal/issues/1157006
   *
   * @return array|null
   *   Node ids if matched criteria, otherwise null.
   */
  private function getNotEqualValue(array $filter) {
    $subQuery = new EntityFieldQuery();

    if (in_array($filter['field'], self::$nodeProperties, TRUE)) {
      $subQuery->propertyCondition(
        $filter['field'],
        $filter['value'],
        '='
      );
    }
    else {
      $subQuery->fieldCondition(
        $filter['field'],
        'value',
        $filter['value'],
        '='
        );
    }

    $sqResult = $subQuery->execute();
    return empty($sqResult['node']) ? NULL : array_keys($sqResult['node']);
  }

  /**
   * Converts relative path to absolute path in DomDocument.
   *
   * @param \DOMDocument $doc
   *   HTML Document.
   * @param string $tag
   *   Tag name.
   * @param string $attribute
   *   Attribute name.
   */
  private function convertToAbsoluteUrls(DOMDocument $doc, $tag, $attribute) {
    global $base_url;
    $images = $doc->getElementsByTagName($tag);

    /** @var \DOMElement $image */
    foreach ($images as $image) {
      // Convert relative link/image paths to absolute.
      if (!url_is_external($image->getAttribute($attribute))) {
        $url = $base_url . $image->getAttribute($attribute);
        $image->setAttribute($attribute, $url);
      }
    }
  }

}
