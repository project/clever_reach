<?php

use CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerWakeup;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;

/**
 * TaskQueue helper class.
 */
class CleverReachTaskQueue {

  /**
   * Enqueues a task to the queue.
   *
   * @param \CleverReach\Infrastructure\TaskExecution\Task $task
   *   Task that needs to be enqueued.
   * @param bool $throwException
   *   Indicates whether exception should be thrown or not.
   *
   * @see \CleverReach\BusinessLogic\Sync
   *
   * @throws Exception
   */
  public static function enqueue(Task $task, $throwException = FALSE) {
    try {
      $configService = ServiceRegister::getService(Configuration::CLASS_NAME);
      $accessToken = $configService->getAccessToken();

      if (!empty($accessToken)) {
        /** @var \CleverReach\Infrastructure\TaskExecution\Queue $queueService */
        $queueService = ServiceRegister::getService(Queue::CLASS_NAME);
        $queueService->enqueue($configService->getQueueName(), $task);
      }
    }
    catch (Exception $e) {
      Logger::logDebug(
        drupal_json_encode(
            array(
              'Message' => 'Failed to enqueue task ' . $task->getType(),
              'ExceptionMessage' => $e->getMessage(),
              'ExceptionTrace' => $e->getTraceAsString(),
              'TaskData' => serialize($task),
            )
        ),
        'Integration'
        );

      if ($throwException) {
        throw $e;
      }
    }
  }

  /**
   * Calls the wakeup on task runner.
   */
  public static function wakeup() {
    /** @var \CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerWakeup $wakeupService */
    $wakeupService = ServiceRegister::getService(TaskRunnerWakeup::CLASS_NAME);
    $wakeupService->wakeup();
  }

}
