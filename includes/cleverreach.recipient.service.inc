<?php

use CleverReach\BusinessLogic\Entity\Recipient;
use CleverReach\BusinessLogic\Entity\SpecialTag;
use CleverReach\BusinessLogic\Entity\SpecialTagCollection;
use CleverReach\BusinessLogic\Entity\Tag;
use CleverReach\BusinessLogic\Entity\TagCollection;
use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;

/**
 * Prepares recipients for export to CleverReach.
 */
class CleverReachRecipientService implements Recipients {
  const TAG_TYPE_ROLE = 'Role';
  const TAG_TYPE_SITE = 'Site';
  const TAG_TYPE_TAXONOMY = 'Taxonomy';

  /**
   * Instance of Configuration class.
   *
   * @var \CleverReach\Infrastructure\Interfaces\Required\Configuration
   */
  private $configService;
  /**
   * Cached list of website tags.
   *
   * @var array
   */
  private $websites = array();
  /**
   * Cached host name.
   *
   * @var string
   */
  private $hostName = '';
  /**
   * Cached site name.
   *
   * @var string
   */
  private $siteName = '';
  /**
   * Cached list of user role tags.
   *
   * @var array
   */
  private $userRoles = array();

  /**
   * Gets all tags as a collection.
   *
   * @return \CleverReach\BusinessLogic\Entity\TagCollection
   *   CleverReach Tag/Segment collection.
   */
  public function getAllTags() {
    $tag = $this->getTagsFormatted(
        $this->getUserRoles(),
        self::TAG_TYPE_ROLE
    );
    $tag->add(
        $this->getTagsFormatted(
            $this->getWebsites(),
            self::TAG_TYPE_SITE
        )
    );
    $tag->add(
        $this->getTagsFormatted(
            $this->getTaxonomies(),
            self::TAG_TYPE_TAXONOMY
        )
    );
    return $tag;
  }

  /**
   * Gets all recipients for passed batch formatted in the proper way.
   *
   * @param array $batchRecipientIds
   *   Array of user IDs.
   * @param bool $includeOrders
   *   Flag that indicates whether orders should be included or not.
   *
   * @return CleverReach\BusinessLogic\Entity\Recipient[]
   *   array of CleverReach\BusinessLogic\Entity\Recipient objects based on
   *   passed ids.
   *   - If includeOrders flag is set to true orders should also be returned
   *     with other recipient data. SPECIAL
   *   - ATTENTION should be pointed towards tags. They should be in instance
   *     specific formatted way.
   *
   * @throws \Exception
   */
  public function getRecipientsWithTags(
        array $batchRecipientIds,
        $includeOrders
    ) {
    $result = array();

    if (empty($batchRecipientIds)) {
      return $result;
    }

    $users = user_load_multiple($batchRecipientIds);
    /** @var stdClass $user */
    foreach ($batchRecipientIds as $recipientId) {
      $user = isset($users[(int) $recipientId]) ? $users[(int) $recipientId] : NULL;
      if ($user === NULL && !is_numeric($recipientId)) {
        continue;
      }

      $result[] = $this->createRecipientFromUser($user);
    }

    return $result;
  }

  /**
   * Gets all user IDs from system.
   *
   * @return array
   *   Array of user IDs.
   */
  public function getAllRecipientsIds() {
    return clever_reach_get_all_user_ids();
  }

  /**
   * Informs service about completed synchronization of provided user (IDs).
   *
   * @param array $recipientIds
   *   List of user IDs.
   */
  public function recipientSyncCompleted(array $recipientIds) {
    // Intentionally left empty. We do not need this functionality.
  }

  /**
   * Gets all special tags as a collection.
   *
   * @return \CleverReach\BusinessLogic\Entity\SpecialTagCollection
   *   Collection of special tags.
   */
  public function getAllSpecialTags() {
    return new SpecialTagCollection(
        array(
          SpecialTag::subscriber(),
          SpecialTag::contact(),
        )
    );
  }

  /**
   * Gets formatted tag collection.
   *
   * @param array $sourceTags
   *   Array of tags.
   * @param string $type
   *   Tag type.
   *
   * @return \CleverReach\BusinessLogic\Entity\TagCollection
   *   Collection of Tag objects.
   */
  private function getTagsFormatted(array $sourceTags, $type) {
    $tagCollection = new TagCollection();
    foreach ($sourceTags as $sourceTag) {
      $tag = new Tag($sourceTag, $type);
      $tagCollection->addTag($tag);
    }
    return $tagCollection;
  }

  /**
   * Creates recipient object from user object.
   *
   * @param object $user
   *   User entity.
   *
   * @return \CleverReach\BusinessLogic\Entity\Recipient
   *   Recipient object.
   *
   * @throws \Exception
   */
  private function createRecipientFromUser($user) {
    $recipient = new Recipient($user->mail);
    $recipient->setCustomerNumber($user->uid);

    $date = new \DateTime();
    $date->setTimestamp($user->created);

    if (NULL !== $date) {
      $recipient->setActivated($date);
      $recipient->setRegistered($date);
    }

    $isSubscribed = $this->isSubscribed($user);
    $recipient->setFirstName($user->name);
    $recipient->setLanguage($user->language);
    $recipient->setActive($isSubscribed);
    $recipient->setNewsletterSubscription($isSubscribed);
    $recipient->setShop($this->getSiteName());
    $recipient->setSource($this->getSource());

    $this->setRecipientTags($user, $recipient);
    $this->setRecipientSpecialTags($recipient);

    return $recipient;
  }

  /**
   * Gets host name of current Drupal instance.
   *
   * @return string
   *   Host name extracted from base url.
   */
  private function getSource() {
    if (empty($this->hostName)) {
      $this->hostName = parse_url(
        url(NULL, array('absolute' => TRUE)),
        PHP_URL_HOST
      );
    }

    return $this->hostName;
  }

  /**
   * Gets site name from configuration.
   *
   * @return string
   *   Site name.
   */
  private function getSiteName() {
    if (empty($this->siteName)) {
      $this->siteName = $this->getConfigService()->getSiteName();
    }

    return $this->siteName;
  }

  /**
   * Gets CleverReach configuration service.
   *
   * @return CleverReachConfigService
   *   Configuration service instance.
   */
  private function getConfigService() {
    if (NULL === $this->configService) {
      $this->configService = ServiceRegister::getService(
        Configuration::CLASS_NAME
      );
    }

    return $this->configService;
  }

  /**
   * Checks if user is subscribed to newsletter.
   *
   * If newsletter field is not set on user entity, fallback value defined
   * before initial sync is used.
   *
   * @param object $user
   *   User object.
   *
   * @return bool
   *   If user is subscribed, returns true, otherwise false.
   */
  private function isSubscribed($user) {
    if (
        !is_array($user->data) ||
        !array_key_exists('field_cleverreach_subscribed', $user->data)
    ) {
      return $this->getConfigService()->getDefaultRecipientStatus();
    }

    return $user->data['field_cleverreach_subscribed'] === 1;
  }

  /**
   * Gets all user prefixed roles / groups.
   *
   * @return array
   *   Array of role names as value and role id as key.
   */
  private function getUserRoles() {
    if (empty($this->userRoles)) {
      $this->userRoles = user_roles(TRUE);
    }

    return $this->userRoles;
  }

  /**
   * Gets all taxonomies defined on user.
   *
   * @param object $user
   *   User object.
   *
   * @return array
   *   List of all taxonomies defined on user.
   */
  private function getUserTaxonomies($user) {
    $taxonomies = array();
    $fields = clever_reach_get_taxonomy_user_fields();

    foreach ($fields as $fieldName => $field) {
      if (!isset($user->{$fieldName}[LANGUAGE_NONE])) {
        continue;
      }

      foreach ($user->{$fieldName}[LANGUAGE_NONE] as $value) {
        if (!$term = taxonomy_term_load($value['tid'])) {
          continue;
        }

        $taxonomies[] = $term->name;
      }
    }

    return array_unique($taxonomies);
  }

  /**
   * Gets array of website tags.
   *
   * @return array
   *   Array of site names.
   */
  private function getWebsites() {
    if (empty($this->websites)) {
      $this->websites = array($this->getConfigService()->getSiteName());
    }

    return $this->websites;
  }

  /**
   * Gets all taxonomy tags defined for user entity.
   *
   * @return array
   *   List of all taxonomies defined on user.
   */
  private function getTaxonomies() {
    return clever_reach_get_taxonomy_user_values();
  }

  /**
   * Sets tags on recipient.
   *
   * @param object $user
   *   User object.
   * @param \CleverReach\BusinessLogic\Entity\Recipient $recipient
   *   Recipient object.
   */
  private function setRecipientTags($user, Recipient $recipient) {
    $tags = $this->getTagsFormatted(
        $user->roles,
        self::TAG_TYPE_ROLE
    );

    $tags->add(
        $this->getTagsFormatted(
            $this->getWebsites(),
            self::TAG_TYPE_SITE
        )
    );

    $tags->add(
        $this->getTagsFormatted(
            $this->getUserTaxonomies($user),
            self::TAG_TYPE_TAXONOMY
        )
    );

    $recipient->setTags($tags);
  }

  /**
   * Sets special tag for recipient.
   *
   * @param \CleverReach\BusinessLogic\Entity\Recipient $recipient
   *   Recipient object.
   */
  private function setRecipientSpecialTags(Recipient $recipient) {
    $specialTags = new SpecialTagCollection(array(SpecialTag::contact()));

    if ($recipient->getNewsletterSubscription()) {
      $specialTags->addTag(SpecialTag::subscriber());
    }

    $recipient->setSpecialTags($specialTags);
  }

}
