<?php

use CleverReach\Infrastructure\Interfaces\Required\ConfigRepositoryInterface;

/**
 * Configuration repository.
 *
 * @see \CleverReach\Infrastructure\Interfaces\Required\ConfigRepositoryInterface
 */
class CleverReachConfigRepository implements ConfigRepositoryInterface {

  /**
   * Sets key-value pair to Drupal configuration table.
   *
   * @param string $key
   *   Identifier to store value in configuration.
   * @param mixed $value
   *   Value to associate with identifier.
   *
   * @throws Exception
   */
  public function set($key, $value) {
    if (!$this->get('CLEVERREACH_INSTALLED')) {
      // Kill all processes as soon as module uninstall is detected.
      throw new Exception(
        'CleverReach module is not currently installed, abort all processes.'
      );
    }

    variable_set(strtolower($key), $value);
  }

  /**
   * Gets configuration by key from Drupal configuration table.
   *
   * @param string $key
   *   Identifier to store value in configuration.
   *
   * @return mixed|null
   *   Configuration value.
   */
  public function get($key) {
    return variable_get(strtolower($key));
  }

}
