<?php

/**
 * Implementation of schema field factory.
 */
class CleverReachArticleSearchField {
  /**
   * List of supported Drupal fields.
   *
   * @var array
   */
  private static $supportedFieldTypes = array(
    'text' => array(
      'class' => 'CleverReachTextType',
      'file' => 'cleverreach.text.type',
    ),
    'text_long' => array(
      'class' => 'CleverReachTextType',
      'file' => 'cleverreach.text.type',
    ),
    'text_with_summary' => array(
      'class' => 'CleverReachTextWithSummaryType',
      'file' => 'cleverreach.text.with.summary.type',
    ),
    'number_decimal' => array(
      'class' => 'CleverReachNumberType',
      'file' => 'cleverreach.number.type',
    ),
    'number_integer' => array(
      'class' => 'CleverReachNumberType',
      'file' => 'cleverreach.number.type',
    ),
    'number_float' => array(
      'class' => 'CleverReachNumberType',
      'file' => 'cleverreach.number.type',
    ),
    'image' => array(
      'class' => 'CleverReachImageType',
      'file' => 'cleverreach.image.type',
    ),
    'list_boolean' => array(
      'class' => 'CleverReachListType',
      'file' => 'cleverreach.list.type',
    ),
    'list_float' => array(
      'class' => 'CleverReachListType',
      'file' => 'cleverreach.list.type',
    ),
    'list_integer'  => array(
      'class' => 'CleverReachListType',
      'file' => 'cleverreach.list.type',
    ),
    'list_text' => array(
      'class' => 'CleverReachListType',
      'file' => 'cleverreach.list.type',
    ),
    'taxonomy_term_reference' => array(
      'class' => 'CleverReachTaxonomyType',
      'file' => 'cleverreach.taxonomy.type',
    ),
  );

  /**
   * Gets instance of field configuration type.
   *
   * @param array $field
   *   Field storage information.
   * @param array $info
   *   Field instance and display information.
   *
   * @return CleverReachBaseField|null
   *   If null is returned, field not supported, otherwise returns instance of
   *   schema field.
   */
  public static function getField(array $field, array $info = array()) {
    if (!self::isFieldTypeSupported($field)) {
      return NULL;
    }

    $fieldMeta = self::$supportedFieldTypes[$field['type']];
    $class = $fieldMeta['class'];

    if (!class_exists('CleverReachBaseField')) {
      module_load_include(
          'inc',
          'clever_reach',
          'includes/fields/cleverreach.base.type'
      );
    }

    if (!class_exists($class)) {
      module_load_include(
          'inc',
          'clever_reach',
          'includes/fields/' . $fieldMeta['file']
      );
    }

    $field['instance'] = $info;

    return new $class($field);
  }

  /**
   * Checks if field is supported by module.
   *
   * @param array $field
   *   Keyed array of field storage information.
   *
   * @return bool
   *   If excluded, returns true, otherwise false.
   */
  private static function isFieldTypeSupported(array &$field) {
    return isset($field['type']) &&
        array_key_exists($field['type'], self::$supportedFieldTypes);
  }

}
