<?php

use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemSaveException;
use CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage;
use CleverReach\Infrastructure\TaskExecution\QueueItem;

/**
 * Task queue storage service implementation.
 *
 * @see \CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage
 */
class CleverReachTaskQueueStorageService implements TaskQueueStorage {
  const TABLE_NAME = 'cleverreach_queue';
  const TABLE_PK = 'id';

  /**
   * Creates or updates given queue item.
   *
   * If queue item id is not set, new queue item will be created otherwise
   * update will be performed.
   *
   * @param \CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem
   *   Item to save.
   * @param array $additionalWhere
   *   List of key/value pairs that must be satisfied upon saving queue item.
   *   Key is queue item property and value is condition value for that
   *   property. Example for MySql storage:
   *   $storage->save($queueItem, array('status' => 'queued')) should produce:
   *     UPDATE queue_storage_table SET .... WHERE .... AND status => 'queued'.
   *
   * @return int
   *   ID of saved queue item.
   *
   * @throws QueueItemSaveException
   *   If queue item could not be saved.
   */
  public function save(QueueItem $queueItem, array $additionalWhere = array()) {
    $itemId = NULL;
    try {
      $queueItemId = $queueItem->getId();
      if (NULL === $queueItemId || $queueItemId <= 0) {
        $itemId = db_insert(self::TABLE_NAME)->fields($this->queueItemToArray($queueItem))->execute();
      }
      else {
        $this->updateQueueItem($queueItem, $additionalWhere);
        $itemId = $queueItemId;
      }
    }
    catch (\Exception $exception) {
      throw new QueueItemSaveException(
        'Failed to save queue item with id: ' . $itemId, 0, $exception
        );
    }

    return $itemId;
  }

  /**
   * Finds queue item by ID.
   *
   * @param int $id
   *   Id of a queue item to find.
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem|null
   *   Found queue item or null when queue item does not exist.
   */
  public function find($id) {
    $queueItem = $this->queueItemFromArray($this->findById($id));
    if ($queueItem === NULL) {
      Logger::logDebug(
        drupal_json_encode(
            array('Message' => "Failed to fetch queue item with id: $id. Queue item  does not exist.")
        )
      );
    }

    return $queueItem;
  }

  /**
   * Finds latest queue item by type across all queues.
   *
   * @param string $type
   *   Type of a queue item to find.
   * @param string $context
   *   Task context restriction if provided search will be limited to given
   *   task context. Leave empty for search across all task contexts.
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem|null
   *   Found queue item or null when queue item does not exist.
   */
  public function findLatestByType($type, $context = '') {
    $latest = $this->findOne(
        array('type' => $type),
        array('queue_timestamp' => TaskQueueStorage::SORT_DESC)
    );
    $queueItem = $this->queueItemFromArray($latest);
    if ($queueItem === NULL) {
      Logger::logDebug(
        drupal_json_encode(
            array('Message' => "Failed to fetch queue item with type: $type. Queue item  does not exist.")
        )
      );
    }

    return $queueItem;
  }

  /**
   * Finds list of earliest queued queue items per queue.
   *
   * Following list of criteria for searching must be satisfied:
   *  - Queue must be without already running queue items.
   *  - For one queue only one (oldest queued) item should be returned.
   *
   * @param int $limit
   *   Result set limit. By default 10 earliest queue items will be
   *    returned.
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem[]
   *   Found queue item list.
   */
  public function findOldestQueuedItems($limit = 10) {
    // Get running queues names.
    $runningQuery = db_select(self::TABLE_NAME);
    $runningQuery->fields(self::TABLE_NAME, array('queue_name'))->condition('status', 'in_progress');

    if (!$execute = $runningQuery->execute()) {
      return array();
    }

    $result = $execute->fetchAssoc();
    if (!empty($result)) {
      return array();
    }

    // Get non-running queues names.
    $nonRunningQuery = db_select(self::TABLE_NAME);
    $nonRunningQuery->fields(self::TABLE_NAME)->condition('status', 'queued');
    $nonRunningQuery->orderBy('queue_timestamp');
    $nonRunningQuery->range(0, $limit);

    if (!$execute = $nonRunningQuery->execute()) {
      return array();
    }

    $result = $execute->fetchAllAssoc(self::TABLE_PK, PDO::FETCH_ASSOC);

    return $this->queueItemsFromArray($result);
  }

  /**
   * Finds all queue items from all queues.
   *
   * @param array $filterBy
   *   List of simple search filters, where key is queue item property and value
   *   is condition value for that property. Leave empty for unfiltered result.
   * @param array $sortBy
   *   List of sorting options where key is queue item property and value sort
   *   direction ("ASC" or "DESC"). Leave empty for default sorting.
   * @param int $start
   *   From which record index result set should start.
   * @param int $limit
   *   Max number of records that should be returned (default is 10).
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem[]
   *   Found queue item list.
   */
  public function findAll(array $filterBy = array(), array $sortBy = array(), $start = 0, $limit = 10) {
    return $this->queueItemsFromArray($this->findAllByCriteria($filterBy, $sortBy, (int) $start, (int) $limit));
  }

  /**
   * Finds record by primary key.
   *
   * @param int $id
   *   Id of record that is searched.
   *
   * @return array|null
   *   If record is found return item in array format, otherwise if not found
   *   returns null.
   */
  public function findById($id) {
    return $this->findOne(array(static::TABLE_PK => $id));
  }

  /**
   * Finds one record by provided conditions.
   *
   * @param array|null $filterBy
   *   List of simple search filters as key-value pair. Leave empty for
   *   unfiltered result.
   * @param array|null $sortBy
   *   List of sorting options where key is field and value is sort direction
   *   ("ASC" or "DESC"). Leave empty for default sorting.
   *
   * @return array|null
   *   If record is found return item in array format, otherwise if not found
   *   returns null.
   */
  private function findOne($filterBy = NULL, $sortBy = NULL) {
    $item = $this->findAllByCriteria($filterBy, $sortBy, 0, 1);
    return !empty($item) ? reset($item) : NULL;
  }

  /**
   * Updates database record with data from provided $queueItem.
   *
   * @param \CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem
   *   Queue item object.
   * @param array $conditions
   *   Array of conditions that must be satisfied when updating records.
   *
   * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemSaveException
   *   When queue item couldn't be saved in database.
   */
  private function updateQueueItem(QueueItem $queueItem, array $conditions = array()) {
    // CORE can give additional conditions to preserve updating item that is
    // possibly locked or already updated this is done because of update lock.
    $conditions = array_merge($conditions, array('id' => $queueItem->getId()));

    $item = $this->findOne($conditions);
    if (empty($item)) {
      $message = 'Failed to save queue item, update condition(s) not met.';
      Logger::logDebug(
        drupal_json_encode(
            array(
              'Message' => $message,
              'WhereCondition' => $conditions,
            )
        )
      );
      throw new QueueItemSaveException($message);
    }

    $item = array_merge($item, $this->queueItemToArray($queueItem));
    $database = db_update(static::TABLE_NAME)->fields($item);

    foreach ($conditions as $field => $value) {
      $database->condition($this->toUnderscoreCase($field), $value, $value === NULL ? 'IS NULL' : '=');
    }

    $database->execute();
  }

  /**
   * Serializes instance of QueueItem to array of values.
   *
   * @param \CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem
   *   QueueItem object.
   *
   * @return array
   *   Array representation of queue item.
   */
  private function queueItemToArray(QueueItem $queueItem) {
    return array(
      'id' => $queueItem->getId(),
      'type' => $queueItem->getTaskType(),
      'status' => $queueItem->getStatus(),
      'queue_name' => $queueItem->getQueueName(),
      'progress' => $queueItem->getProgressFormatted(),
      'last_execution_progress' => $queueItem->getLastExecutionProgressBasePoints(),
      'retries' => $queueItem->getRetries(),
      'failure_description' => $queueItem->getFailureDescription(),
      'serialized_task' => $queueItem->getSerializedTask(),
      'create_timestamp' => $queueItem->getCreateTimestamp(),
      'queue_timestamp' => $queueItem->getQueueTimestamp(),
      'last_update_timestamp' => $queueItem->getLastUpdateTimestamp(),
      'start_timestamp' => $queueItem->getStartTimestamp(),
      'finish_timestamp' => $queueItem->getFinishTimestamp(),
      'fail_timestamp' => $queueItem->getFailTimestamp(),
    );
  }

  /**
   * Transforms array of values to QueueItem instance.
   *
   * @param array|null $item
   *   Array representation of queue item.
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem
   *   Object representation of queue item.
   */
  private function queueItemFromArray($item) {
    if (empty($item)) {
      return NULL;
    }

    $queueItem = new QueueItem();
    $queueItem->setId((int) $item['id']);
    $queueItem->setStatus($item['status']);
    $queueItem->setQueueName($item['queue_name']);
    $queueItem->setProgressBasePoints((int) $item['progress']);
    $queueItem->setLastExecutionProgressBasePoints((int) $item['last_execution_progress']);
    $queueItem->setRetries((int) $item['retries']);
    $queueItem->setFailureDescription($item['failure_description']);
    $queueItem->setSerializedTask($item['serialized_task']);
    $queueItem->setCreateTimestamp((int) $item['create_timestamp']);
    $queueItem->setQueueTimestamp((int) $item['queue_timestamp']);
    $queueItem->setLastUpdateTimestamp((int) $item['last_update_timestamp']);
    $queueItem->setStartTimestamp((int) $item['start_timestamp']);
    $queueItem->setFinishTimestamp((int) $item['finish_timestamp']);
    $queueItem->setFailTimestamp((int) $item['fail_timestamp']);

    return $queueItem;
  }

  /**
   * Transforms array of values to array of QueueItem instances.
   *
   * @param array|null $items
   *   List of items that need to be converted to list of QueueItem objects.
   *
   * @return \CleverReach\Infrastructure\TaskExecution\QueueItem[]
   *   List of QueueItem objects.
   */
  private function queueItemsFromArray($items) {
    $result = array();
    if (empty($items)) {
      return $result;
    }

    foreach ($items as $item) {
      $result[] = $this->queueItemFromArray($item);
    }

    return $result;
  }

  /**
   * Finds all records for provided conditions ordered in provided sort.
   *
   * @param array|null $filterBy
   *   List of simple search filters as key-value pair. Leave empty for
   *   unfiltered result.
   * @param array|null $sortBy
   *   List of sorting options where key is field and value is sort direction
   *   ("ASC" or "DESC"). Leave empty for default sorting.
   * @param int $start
   *   From which record index result set should start.
   * @param int $limit
   *   Max number of records that should be returned (default is 10)
   * @param array|null $select
   *   List of table columns to return. Column names could have alias as well.
   *   If empty, all columns are returned.
   *
   * @return array
   *   Array of matched records.
   */
  private function findAllByCriteria($filterBy = NULL, $sortBy = NULL, $start = 0, $limit = 0, $select = NULL) {
    $query = db_select(static::TABLE_NAME)->fields(static::TABLE_NAME, $select === NULL ? array() : $select);

    if (is_array($filterBy)) {
      foreach ($filterBy as $field => $value) {
        $query->condition($this->toUnderscoreCase($field), $value, $value === NULL ? 'IS NULL' : '=');
      }
    }

    if (is_array($sortBy)) {
      foreach ($sortBy as $sortField => $direction) {
        $query->orderBy($this->toUnderscoreCase($sortField), $direction);
      }
    }

    if ($limit > 0) {
      $query->range($start, $limit);
    }

    if (!$execute = $query->execute()) {
      return NULL;
    }

    return $execute->fetchAllAssoc(static::TABLE_PK, PDO::FETCH_ASSOC);
  }

  /**
   * Helper method that performs converting of camel case to underscore case.
   *
   * @param string $input
   *   String to be converted to underscore case.
   *
   * @return string
   *   Camelcase string representation.
   */
  private function toUnderscoreCase($input) {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match === strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }

}
