<?php

use CleverReach\BusinessLogic\Entity\RecipientAttribute;
use CleverReach\BusinessLogic\Interfaces\Attributes;

/**
 * Get attribute from Drupal with translated description in default language.
 */
class CleverReachAttributesService implements Attributes {

  /**
   * Attribute mapping [attribute_name => drupal_attribute_label].
   *
   * @var array
   */
  private static $attributes = array(
    'email' => 'E-mail address',
    'firstname' => 'Username',
    'shop' => 'Site name',
    'customernumber' => 'UID',
    'language' => 'Preferred language',
    'newsletter' => 'Subscribed to newsletter',
  );

  /**
   * Get attributes from integration with translated params in system language.
   *
   * It should set name, description, preview_value and default_value for each
   * attribute available in system.
   *
   * @return \CleverReach\BusinessLogic\Entity\RecipientAttribute[]
   *   List of available attributes in the system.
   */
  public function getAttributes() {
    $attributes = array();
    foreach (self::$attributes as $attributeName => $attributeLabel) {
      $recipientAttribute = new RecipientAttribute($attributeName);
      $recipientAttribute->setDescription(t($attributeLabel));
      $attributes[] = $recipientAttribute;
    }

    return $attributes;
  }

}
