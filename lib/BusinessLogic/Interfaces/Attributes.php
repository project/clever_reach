<?php

namespace CleverReach\BusinessLogic\Interfaces;

/**
 * Interface Attributes.
 *
 * @package CleverReach\BusinessLogic\Interfaces
 */
interface Attributes {

  const CLASS_NAME = __CLASS__;

  /**
   * Get attributes from integration with translated params in system language.
   *
   * It should set name, description, preview_value and default_value for each attribute available in system.
   *
   * @return \CleverReach\BusinessLogic\Entity\RecipientAttribute[]
   *   List of available attributes in the system.
   */
  public function getAttributes();

}
