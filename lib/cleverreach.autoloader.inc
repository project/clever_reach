<?php

use CleverReach\BusinessLogic\Interfaces\Attributes;
use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\BusinessLogic\Proxy;
use CleverReach\BusinessLogic\Interfaces\Proxy as ProxyInterface;
use CleverReach\Infrastructure\Interfaces\DefaultLoggerAdapter;
use CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerWakeup as TaskRunnerWakeUpInterface;
use CleverReach\Infrastructure\Interfaces\Required\AsyncProcessStarter;
use CleverReach\Infrastructure\Interfaces\Required\ConfigRepositoryInterface;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Interfaces\Required\HttpClient;
use CleverReach\Infrastructure\Interfaces\Required\ShopLoggerAdapter;
use CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage;
use CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerStatusStorage as TaskRunnerStatusStorageInterface;
use CleverReach\Infrastructure\Logger\DefaultLogger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\TaskExecution\TaskRunner;
use CleverReach\Infrastructure\TaskExecution\TaskRunnerStatusStorage;
use CleverReach\Infrastructure\TaskExecution\TaskRunnerWakeup;
use CleverReach\Infrastructure\Utility\GuidProvider;
use CleverReach\Infrastructure\Utility\TimeProvider;

/**
 * Class CleverReachAutoloader.
 */
class CleverReachAutoloader {
  const CORE_NAMESPACE = 'CleverReach\\';

  /**
   * Indicator if core has been loaded.
   *
   * @var bool
   */
  private static $loaded = FALSE;

  /**
   * Loads CleverReach core and register core services.
   *
   * @throws Exception
   */
  public function load() {
    if (self::$loaded === FALSE) {
      $this->registerClasses();
      $this->registerServices();
      self::$loaded = TRUE;
    }
  }

  /**
   * Load CleverReach core.
   */
  private function registerClasses() {
    spl_autoload_register(array($this, 'loadClass'), TRUE, TRUE);
  }

  /**
   * Registers all core services.
   *
   * @throws Exception
   */
  private function registerServices() {
    ServiceRegister::registerService(
        TimeProvider::CLASS_NAME,
        function () {
            return new TimeProvider();
        }
    );
    ServiceRegister::registerService(
        Queue::CLASS_NAME,
        function () {
            return new Queue();
        }
    );
    ServiceRegister::registerService(
        ProxyInterface::CLASS_NAME,
        function () {
            return new Proxy();
        }
    );
    ServiceRegister::registerService(
        TaskRunnerWakeUpInterface::CLASS_NAME,
        function () {
            return new TaskRunnerWakeup();
        }
    );
    ServiceRegister::registerService(
        TaskRunner::CLASS_NAME,
        function () {
            return new TaskRunner();
        }
    );
    ServiceRegister::registerService(
        GuidProvider::CLASS_NAME,
        function () {
            return new GuidProvider();
        }
    );
    ServiceRegister::registerService(
        DefaultLoggerAdapter::CLASS_NAME,
        function () {
            return new DefaultLogger();
        }
    );
    ServiceRegister::registerService(
        TaskRunnerStatusStorageInterface::CLASS_NAME,
        function () {
            return new TaskRunnerStatusStorage();
        }
    );
    ServiceRegister::registerService(
        Configuration::CLASS_NAME,
        function () {
            return new CleverReachConfigService();
        }
    );
    ServiceRegister::registerService(
        ConfigRepositoryInterface::CLASS_NAME,
        function () {
            return new CleverReachConfigRepository();
        }
    );
    ServiceRegister::registerService(
        HttpClient::CLASS_NAME,
        function () {
            return new CleverReachHttpClientService();
        }
    );
    ServiceRegister::registerService(
        ShopLoggerAdapter::CLASS_NAME,
        function () {
            return new CleverReachLoggerService();
        }
    );
    ServiceRegister::registerService(
        AsyncProcessStarter::CLASS_NAME,
        function () {
            return new CleverReachAsyncProcessStarterService();
        }
    );
    ServiceRegister::registerService(
        TaskQueueStorage::CLASS_NAME,
        function () {
            return new CleverReachTaskQueueStorageService();
        }
    );
    ServiceRegister::registerService(
        Attributes::CLASS_NAME,
        function () {
            return new CleverReachAttributesService();
        }
    );
    ServiceRegister::registerService(
        Recipients::CLASS_NAME,
        function () {
            return new CleverReachRecipientService();
        }
    );
  }

  /**
   * Callback function for spl_autoload_register method. Loads single class.
   *
   * @param string $class
   *   Class load name.
   */
  private function loadClass($class) {
    if (strpos($class, self::CORE_NAMESPACE) === 0) {
      $this->requireFile($class, self::CORE_NAMESPACE);
    }
  }

  /**
   * Includes file if exist.
   *
   * @param string $class
   *   Class name.
   * @param string $namespace
   *   Namespace.
   */
  private function requireFile($class, $namespace) {
    $class = str_replace(array($namespace, '\\'), array('', '/'), $class);
    $file = __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';

    if (file_exists($file)) {
      require_once $file;
    }
  }

}
