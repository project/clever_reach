<?php

namespace CleverReach\Infrastructure\Interfaces;

/**
 * Interface LoggerAdapter.
 *
 * @package CleverReach\Infrastructure\Interfaces
 */
interface LoggerAdapter {

  /**
   * Log message in the system.
   *
   * @param \CleverReach\Infrastructure\Logger\LogData|null $data
   *   Log data object.
   */
  public function logMessage($data);

}
