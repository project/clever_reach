<?php

namespace CleverReach\Infrastructure\TaskExecution;

use CleverReach\Infrastructure\Interfaces\Exposed\Runnable;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;

/**
 * Class QueueItemStarter.
 *
 * @package CleverReach\Infrastructure\TaskExecution
 */
class QueueItemStarter implements Runnable {
  /**
   * ID of queue item to start.
   *
   * @var int
   */
  private $queueItemId;
  /**
   * Queue instance.
   *
   * @var Queue
   */
  private $queue;
  /**
   * Configuration service instance.
   *
   * @var \CleverReach\Infrastructure\Interfaces\Required\Configuration
   */
  private $configService;

  /**
   * QueueItemStarter constructor.
   *
   * @param int $queueItemId
   *   ID of queue item to start.
   */
  public function __construct($queueItemId) {
    $this->queueItemId = $queueItemId;
  }

  /**
   * String representation of object.
   *
   * @inheritdoc
   */
  public function serialize() {
    return serialize(array($this->queueItemId));
  }

  /**
   * Constructs the object.
   *
   * @inheritdoc
   */
  public function unserialize($serialized) {
    list($this->queueItemId) = unserialize($serialized);
  }

  /**
   * Starts runnable run logic.
   *
   * @throws Exceptions\QueueStorageUnavailableException
   */
  public function run() {
    /** @var \CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem */
    $queueItem = $this->fetchItem();

    if ($queueItem === NULL || ($queueItem->getStatus() !== QueueItem::QUEUED)) {
      Logger::logDebug(
        json_encode(array(
          'Message' => 'Fail to start task execution because task no longer exists or it is not in queued state anymore.',
          'TaskId' => $this->getQueueItemId(),
          'Status' => $queueItem !== NULL ? $queueItem->getStatus() : 'unknown',
        ))
      );

      return;
    }

    try {
      $this->getConfigService()->setContext($queueItem->getContext());
      $this->getQueueService()->start($queueItem);
      $this->getQueueService()->finish($queueItem);
    }
    catch (\Exception $ex) {
      if (QueueItem::IN_PROGRESS === $queueItem->getStatus()) {
        $this->getQueueService()->fail($queueItem, $ex->getMessage());
      }

      Logger::logError(
        json_encode(array(
          'Message' => 'Fail to start task execution.',
          'TaskId' => $this->getQueueItemId(),
          'ExceptionMessage' => $ex->getMessage(),
        ))
        );
      Logger::logDebug(
            json_encode(array(
              'Message' => 'Fail to start task execution.',
              'TaskId' => $this->getQueueItemId(),
              'ExceptionMessage' => $ex->getMessage(),
              'ExceptionTrace' => $ex->getTraceAsString(),
            ))
      );
    }
  }

  /**
   * Gets ID of a queue item that will be run.
   *
   * @return int
   *   Queue item ID.
   */
  public function getQueueItemId() {
    return $this->queueItemId;
  }

  /**
   * Gets queue item by ID.
   *
   * @return QueueItem|null
   *   If not found null is returned.
   */
  private function fetchItem() {
    $queueItem = NULL;

    try {
      $queueItem = $this->getQueueService()->find($this->queueItemId);
    }
    catch (\Exception $ex) {
      Logger::logError(json_encode(array(
        'Message' => 'Fail to start task execution.',
        'TaskId' => $this->getQueueItemId(),
        'ExceptionMessage' => $ex->getMessage(),
      )));
      Logger::logDebug(json_encode(array(
        'Message' => 'Fail to start task execution.',
        'TaskId' => $this->getQueueItemId(),
        'ExceptionMessage' => $ex->getMessage(),
        'ExceptionTrace' => $ex->getTraceAsString(),
      )));
    }

    return $queueItem;
  }

  /**
   * Gets queue service instance.
   *
   * @return Queue
   *   Instance of queue service.
   */
  private function getQueueService() {
    if ($this->queue === NULL) {
      $this->queue = ServiceRegister::getService(Queue::CLASS_NAME);
    }

    return $this->queue;
  }

  /**
   * Gets configuration service instance.
   *
   * @return \CleverReach\Infrastructure\Interfaces\Required\Configuration
   *   Instance of configuration service.
   */
  private function getConfigService() {
    if ($this->configService === NULL) {
      $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
    }

    return $this->configService;
  }

}
