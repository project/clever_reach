<?php

/**
 * @file
 * Dashboard state template. Show first email, next email or failed message.
 */
?>
<div class="cr-container">
    <input type="hidden" id="cr-build-email-url"
           value="<?php print $urls['built_email_url']; ?>">
    <input type="hidden" id="cr-build-first-email-url"
           value="<?php print $urls['built_first_email_url']; ?>">
    <input type="hidden" id="cr-retry-sync-url"
           value="<?php print $urls['retry_sync_url']; ?>">

    <div class="cr-tab-wrapper">
        <div class="cr-dashboard-tab-wrapper">
            <button class="cr-dashboard-tab" tabindex="0"
                    role="button"><?php print t('Dashboard'); ?></button>
        </div>
        <div class="cr-pull-right">
            <a href="<?php print t('https://support.cleverreach.de/hc/en-us'); ?>"
               target="_blank"><?php print t('Help & Support'); ?></a>
            <p class="cr-id"><?php print t('CleverReach® ID'); ?>
                : <?php print $recipient_id; ?></p>
        </div>
    </div>

    <div class="cr-content-window-wrapper">
      <?php if ($is_initial_sync_failed) : ?>
          <div class="cr-content-window">
              <img class="cr-icon" src="<?php print $urls['logo_url']; ?>"/>
              <h3>
                <?php print t('An error occurred!'); ?>
              </h3>
              <div class="cr-dashboard-text-wrapper cr-main-text">
                <?php print t('Error description'); ?>
                  : <?php print $initial_sync_failed_message; ?> <br/>
                  <a target="_blank"
                     href="<?php print t('https://support.cleverreach.de/hc/en-us'); ?>"><?php print t('Help & Support'); ?></a>
              </div>
              <a class="cr-action-buttons-wrapper cr-primary" id="cr-retrySync"
                 tabindex="0" role="button">
                <?php print t('Retry synchronization now'); ?>
              </a>
          </div>
      <?php else: ?>
          <div class="cr-dashboard-block">
              <div class="cr-dashboard-container
                    <?php if ($report['isReportEnabled']) : ?>
                    cr-has-import
                    <?php endif; ?>">
                <?php if ($report['isReportEnabled']) : ?>
                    <div class="cr-import">
                        <svg viewBox="0 0 400 150"
                             preserveAspectRatio="xMinYMin meet"
                             style="stroke: none; fill: url('#cr-gradient')">
                            <path d="M0,130 C150,180 200,60 400,120 L400,00 L0,0 Z"
                                  style="stroke: none;"></path>
                            <defs>
                                <linearGradient id="cr-gradient" x1="100%"
                                                y1="0%" x2="0%" y2="100%">
                                    <stop offset="0%"
                                          stop-color="#0AE355"></stop>
                                    <stop offset="72%"
                                          stop-color="#00C562"></stop>
                                </linearGradient>
                            </defs>
                        </svg>
                        <div class="cr-import-successful">
                          <?php print t('Import was successful!') ?>
                        </div>
                        <div class="cr-success-circle">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="cr-report-content">
                            <div class="cr-recipients">
                                <div class="cr-dashboard-report-icon-large">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="cr-dashboard-concrete-report">
                                    <div class="title">
                                      <?php print t('Recipients') ?>
                                    </div>
                                    <div class="value">
                                      <?php print $report['recipients'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="cr-recipient-list">
                                <div class="cr-dashboard-report-icon-large">
                                    <i class="fa fa-clipboard-list"></i>
                                </div>
                                <div class="cr-dashboard-concrete-report">
                                    <div class="title">
                                      <?php print t('Recipient list') ?>
                                    </div>
                                    <div
                                            class="value cr-value-nowrap"
                                            title="<?php print $report['name'] ?>"
                                    >
                                      <?php print $report['name'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="cr-segments">
                                <div class="cr-dashboard-report-icon-small">
                                    <i class="fa fa-tag"></i>
                                </div>
                                <div class="cr-dashboard-concrete-report">
                                    <div class="title">
                                      <?php print t('Segments') ?>
                                    </div>
                                  <?php foreach ($report['tags'] as $tag) : ?>
                                        <?php print $tag ?>
                                  <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="cr-gdpr">
                              <a href="<?php print t('https://www.cleverreach.com/en/features/privacy-security/eu-general-data-protection-regulation-gdpr/') ?>"
                                 class="cr-link" target="_blank">
                                  <i><?php print t('Observe notes on GDPR!') ?></i>
                              </a>
                        </div>
                    </div>
                <?php endif; ?>

                  <!-- DASHBOARD CARD -->
                  <div class="cr-create">
                      <img class="cr-dashboard-logo"
                           src="<?php print $urls['dashboard_logo_url'] ?>"
                           alt="Logo">
                      <h3>
                        <?php print t('Targeted email marketing for more revenue!'); ?>
                      </h3>
                      <div class="cr-dashboard-text-wrapper cr-main-text">
                        <?php print t('Create appealing emails to showcase your articles to your readers'); ?>
                      </div>
                      <div class="cr-button-container">
                          <a class="cr-action-buttons-wrapper cr-primary"
                             id="cr-buildEmail" tabindex="0" role="button">
                            <?php
                            if ($is_built_first_email):
                              ?>
                              <?php print t('Create your next newsletter now!'); ?> →
                            <?php
                            else:
                              ?>
                              <?php print t('Create your first newsletter'); ?> →
                            <?php
                            endif;
                            ?>
                          </a>
                      </div>
                  </div>
                  <!-- /DASHBOARD CARD -->
              </div>
          </div>
      <?php endif; ?>
    </div>
</div>
