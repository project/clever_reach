<?php

/**
 * @file
 * Initial sync state template. Shows current progress of data export.
 */
?>
<div class="cr-container">
    <input type="hidden" id="cr-admin-status-check-url" value="<?php print $urls['check_status_url']; ?>">

    <div data-task-list-panel>
        <ul class="cr-list-group">
            <?php
            foreach ($progress_items as $key => $title):
            ?>
            <li class="cr-list-group-item disabled" data-task="<?php print $key; ?>">
                <div class="cr-content-window-wrapper">
                    <div class="cr-content-window">
                        <i class="cr-icofont cr-icofont-circle">
                            <i class="cr-icofont cr-icofont-2x" aria-hidden="true" data-status="<?php print $key; ?>"></i>
                        </i>
                        <div class="cr-item">
                            <div class="cr-item-text"><?php print $title; ?></div>
                            <div class="cr-item-badge">
                                <span class="cr-progress-text" data-progress="<?php print $key; ?>">0%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>
</div>
