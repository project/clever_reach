<?php

/**
 * @file
 * User defines whether existing subscribers should be synced as active or not.
 */
?>
<div class="cr-container">
    <input type="hidden" id="cr-configuration" value="<?php print $urls['configuration_url']; ?>">

    <div class="cr-tab-wrapper">
        <div class="cr-dashboard-tab-wrapper">
            <button class="cr-dashboard-tab" tabindex="0" role="button"><?php print t('Dashboard'); ?></button>
        </div>
        <div class="cr-pull-right">
            <a href="<?php print t('https://support.cleverreach.de/hc/en-us'); ?>" target="_blank"><?php print t('Help & Support'); ?></a>
            <p class="cr-id"><?php print t('CleverReach® ID'); ?>: <?php print $recipient_id; ?></p>
        </div>
    </div>

    <div class="cr-content-window-wrapper">
        <div class="cr-content-window">
            <img class="cr-icon" src="<?php print $urls['logo_url']; ?>" />

            <div class="cr-configuration">
                <label for="cr-syncAsActive"> <input type="checkbox" id="cr-syncAsActive" />
                    <?php print t('Current users should be synced as subscribers'); ?>
                </label>
                <i><?php print t('If this option is turned on, CleverReach® initial synchronization will threat all users as Subscribers. Every later synchronization will use subscription status that was set to user.'); ?></i>
            </div>

            <a data-success-panel-start-initial-sync class="cr-action-buttons-wrapper cr-primary" id="cr-startSync" tabindex="0" role="button">
                <?php print t('Start synchronization now'); ?> →
            </a>
        </div>
    </div>
</div>
