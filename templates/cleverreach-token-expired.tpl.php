<?php

/**
 * @file
 * Token expired state template.
 *
 * If auth and refresh token expired, reauthenticate the user.
 */
?>
<div class="cr-container">
    <input type="hidden" id="cr-auth-url"
           value="<?php print $urls['auth_url']; ?>">
    <input type="hidden" id="cr-check-status-url"
           value="<?php print $urls['check_status_url']; ?>">
    <input type="hidden" id="cr-wakeup-url"
           value="<?php print $urls['wakeup_url']; ?>">

    <div class="cr-loader-big">
        <span class="cr-loader"></span>
    </div>

    <div class="cr-connecting">
        <span>
            <?php print t('Connecting...'); ?>
        </span>
    </div>

    <div class="cr-content-window-wrapper">
        <div class="cr-content-window">
            <img class="cr-welcome-icon" src="<?php print $urls['logo_url']; ?>"/>
            <h3>
              <?php print t('Error: token expired'); ?>
            </h3>
            <div class="cr-dashboard-text-wrapper cr-main-text">
              <?php print sprintf(
                t('It seems that it was a long time since any event 
                  happened inside your %s and CleverReach® access token expired.
                   In order to let system synchronize data for you, you\'ll 
                   have to authenticate again with CleverReach® ID %s. If you 
                   want to use different CleverReach® account, you need to 
                   reinstall this extension.'),
                $config['integration_name'],
                $config['client_id']
              ); ?> <br/>
            </div>
            <a class="cr-action-buttons-wrapper cr-primary" id="cr-log-account"
               tabindex="0" role="button">
              <?php print t('Authenticate now'); ?>
            </a>
        </div>
    </div>
</div>
