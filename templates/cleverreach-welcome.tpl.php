<?php

/**
 * @file
 * Welcome state template. User authentication to CleverReach screen.
 */
?>
<div class="cr-container">
    <input type="hidden" id="cr-auth-url" value="<?php print $urls['auth_url']; ?>">
    <input type="hidden" id="cr-check-status-url" value="<?php print $urls['check_status_url']; ?>">
    <input type="hidden" id="cr-wakeup-url" value="<?php print $urls['wakeup_url']; ?>">

    <div class="cr-loader-big">
        <span class="cr-loader"></span>
    </div>

    <div class="cr-connecting">
        <span>
            <?php print t('Connecting...'); ?>
        </span>
    </div>

    <div class="cr-content-window-wrapper">
        <div class="cr-content-window">
            <img class="cr-welcome-icon" src="<?php print $urls['logo_url']; ?>">
            <h3><?php print t('Welcome to clever Email Marketing!'); ?></h3>
            <div class="cr-dashboard-text-wrapper cr-main-text">
                <?php print t('Simply transmit Drupal data and get started with CleverReach® for free!'); ?>
            </div>
            <div class="cr-action-buttons-wrapper">
                <a class="cr-primary" id="cr-new-account" tabindex="0" role="button">
                    <?php print t('Create and connect your new CleverReach® account now!'); ?>
                </a>
                <a class="cr-secondary" id="cr-log-account" tabindex="0" role="button">
                    <?php print t('Log in and connect with an existing CleverReach® account'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
